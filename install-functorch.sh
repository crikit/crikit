#!/bin/bash


help()
{
    echo "Installs CPU, CUDA 10.2, or CUDA 11.1 functorch"
    echo 
    echo "Syntax: $0 <cpu,cuda10.2,cuda11.1> [-h]"
    echo "options:"
    echo "h     Print this message."
    echo
}

remove_existing_torch()
{
    pip uninstall torch
}

install_functorch()
{
    pip install ninja
    pip install "git+https://github.com/facebookresearch/functorch.git"
}

install_cpu()
{
    echo "Removing any existing PyTorch installation..."
    remove_existing_torch
    echo "Installing CPU-only functorch..."
    pip install --pre torch -f https://download.pytorch.org/whl/nightly/cpu/torch_nightly.html
    install_functorch
}


install_cuda_102()
{
    echo "Removing any existing PyTorch installation..."
    remove_existing_torch
    echo "Installing CUDA 10.2-enabled functorch..."
    pip install --pre torch -f https://download.pytorch.org/whl/nightly/cu102/torch_nightly.html
    install_functorch
}

install_cuda_111()
{
    echo "Removing any existing PyTorch installation..."
    remove_existing_torch
    echo "Installing CUDA 11.1-enabled functorch..."
    pip install --pre torch -f https://download.pytorch.org/whl/nightly/cu111/torch_nightly.html
    install_functorch
}

while getopts ":h" option; do
  case $option in
     h)
	 help
	 exit;;
     \?)
	 echo "Error: Invalid option"
	 exit 1;;
  esac
done

if [ $# -gt 0 ]; then
    case $1 in
	"cpu")
	    install_cpu
	    ;;
	"cuda10.2")
	    install_cuda_102
	    ;;
	"cuda11.1")
	    install_cuda_111
	    ;;
	*)
	    echo "Invalid functorch compute backend type. Valid backends are {cpu,cuda10.2,cuda11.1}"
	    exit 1
	    ;;
    esac
else
    install_cpu
fi;

	
	    
