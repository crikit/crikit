import tensorflow as tf
from tensorflow.keras import layers


def build_input_convex_network(g_inputs, layer_sizes, num_outputs):
    # Build a Fully Input Convex Network, according to the specs provided by Amos et al.
    #   (Input Convex Neural Networks: https://arxiv.org/pdf/1609.07152.pdf)

    nonneg_init = tf.keras.initializers.RandomUniform(0.0, 0.3, dtype=tf.dtypes.float64)
    normal_init = tf.keras.initializers.RandomUniform(
        -0.3, 0.3, dtype=tf.dtypes.float64
    )
    x_hidden_layer_kwargs = {
        "activation": "linear",
        "kernel_initializer": nonneg_init,
        "bias_initializer": normal_init,
        "kernel_constraint": tf.keras.constraints.NonNeg(),
    }
    y_hidden_layer_kwargs = {
        "activation": "linear",
        "kernel_initializer": normal_init,
        "use_bias": False,
    }

    z = g_inputs
    for num in layer_sizes:
        x = layers.Dense(num, **x_hidden_layer_kwargs)(z)
        y = layers.Dense(num, **y_hidden_layer_kwargs)(g_inputs)
        z = tf.keras.activations.relu(x + y)

    x = layers.Dense(num_outputs, **x_hidden_layer_kwargs)(z)
    y = layers.Dense(num_outputs, **y_hidden_layer_kwargs)(g_inputs)

    g_output = tf.keras.activations.softplus(x + y)
    if num_outputs == 1:
        g_output = tf.reshape(g_output, (-1,))
    return g_output


def build_deep_network(g_inputs, layer_sizes, num_outputs):
    weight_init = tf.keras.initializers.RandomUniform(
        -0.3, 0.3, dtype=tf.dtypes.float64
    )
    hidden_layer_kwargs = {
        "activation": "tanh",
        "kernel_initializer": weight_init,
        "bias_initializer": weight_init,
    }
    x = g_inputs
    for num in layer_sizes:
        x = layers.Dense(num, **hidden_layer_kwargs)(x)

    output_layer_kwargs = {
        "activation": "linear",
        "kernel_initializer": weight_init,
        "bias_initializer": weight_init,
    }
    g_output = layers.Dense(num_outputs, **output_layer_kwargs)(x)
    if num_outputs == 1:
        g_output = tf.reshape(g_output, (-1,))
    return g_output
