import pytest

tf = pytest.importorskip("tensorflow.compat.v1")

import numpy as np
from pyadjoint import *
from pyadjoint_utils import *

try:
    tf.disable_eager_execution()
    from pyadjoint_utils.tensorflow_adjoint import *
except:
    pass

from numpy.testing import assert_approx_equal, assert_allclose


def test_tensorflow_block_adjoint():

    data = AdjFloat(3)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(g_output, {g_data: data, g_p: p})
        J = output

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, Control(p))
        Jhat_both = ReducedFunctional(J, [Control(p), Control(data)])

    assert output == 9

    ### Test data input.
    # Run graph using taped computation.
    assert rf_data(5) == 25

    # Do taylor test.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_data, data, h) > 1.9

    ### Test p input.
    # Run graph using taped computation.
    rf_data(2)  # Set data's block variable back to 2.
    assert rf_p(3) == 8

    # Do taylor test.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_p, p, h) > 1.9

    ### Test data and p inputs.
    # Run graph using taped computation.
    assert rf_both([4, 3]) == 81

    # Do taylor test.
    h = [AdjFloat(1.0), AdjFloat(2.0)]
    assert taylor_test(Jhat_both, [p, data], h) > 1.9

    sess.close()


def test_tensorflow_block_jacobian_action():
    data = AdjFloat(3)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(g_output, {g_data: data, g_p: p})
        J = output

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

    h = AdjFloat(2.0)
    correct_grad_p = h * (data ** p) * np.log(data)
    correct_grad_data = h * (data ** (p - 1)) * p

    grad = rf_p.jac_action(h)
    assert_approx_equal(grad, correct_grad_p)

    grad = rf_data.jac_action(h)
    assert_approx_equal(grad, correct_grad_data)

    grad = rf_both.jac_action([h, 2 * h])
    assert_allclose(grad, correct_grad_p + 2 * correct_grad_data)

    sess.close()


def test_tensorflow_block_jacobian_matrix():
    data = AdjFloat(3)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(g_output, {g_data: data, g_p: p})
        J = output

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

    correct_grad_p = (data ** p) * np.log(data)
    correct_grad_data = (data ** (p - 1)) * p

    jac = rf_p.jac_matrix()
    assert jac.shape == ()
    assert_allclose(jac, [[correct_grad_p]])

    jac = rf_data.jac_matrix()
    assert_allclose(jac, [[correct_grad_data]])

    jac = rf_both.jac_matrix()
    assert len(jac) == 2
    assert jac[0].shape == ()
    assert jac[1].shape == ()
    assert_allclose(jac[0], correct_grad_p)
    assert_allclose(jac[1], correct_grad_data)

    sess.close()


def test_tensorflow_consecutive_blocks():
    data = AdjFloat(3)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        # Run graph twice, computing (data**p)**p ( == data**(p*p))
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(g_output, {g_data: data, g_p: p})
            output = run_tensorflow_graph(g_output, {g_data: output, g_p: p})
        J = output

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, Control(p))
        Jhat_both = ReducedFunctional(J, [Control(p), Control(data)])

    # Test recomputation.
    assert rf_data(4) == 4 ** 4
    assert rf_p(3) == 4 ** 9
    assert rf_both([4, 2]) == 2 ** 16

    # Test adjoint.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_data, data, h) > 1.9
    assert taylor_test(Jhat_p, p, h) > 1.9

    h = [AdjFloat(1.0), AdjFloat(2.0)]
    assert taylor_test(Jhat_both, [p, data], h) > 1.9

    rf_both([p, data])  # Set taped values back to 2 and 3.
    p2 = p * p
    correct_grad_p = (data ** p2) * np.log(data) * 2 * p
    correct_grad_data = (data ** (p2 - 1)) * p2

    # Test jacobian action.
    h = AdjFloat(2.0)
    grad = rf_p.jac_action(h)
    assert_approx_equal(grad, h * correct_grad_p)

    grad = rf_data.jac_action(h)
    assert_approx_equal(grad, h * correct_grad_data)

    grad = rf_both.jac_action([h, 2 * h])
    assert_allclose(grad, h * correct_grad_p + 2 * h * correct_grad_data)

    # Test jacobian matrix.
    jac = rf_p.jac_matrix()
    assert_allclose(jac, [[correct_grad_p]])
    assert isinstance(jac, np.ndarray)
    assert jac.shape == ()

    jac = rf_data.jac_matrix()
    assert_allclose(jac, [[correct_grad_data]])
    assert isinstance(jac, np.ndarray)
    assert jac.shape == ()

    jac = rf_both.jac_matrix()
    assert len(jac) == 2
    assert jac[0].shape == ()
    assert jac[1].shape == ()
    assert_allclose(jac[0], correct_grad_p)
    assert_allclose(jac[1], correct_grad_data)

    sess.close()


def test_tensorflow_variables():
    data = AdjFloat(3)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.get_variable(
                shape=(), initializer=tf.zeros_initializer(), dtype=tf.float64, name="p"
            )

            g_output = g_data ** g_p

            sess = tf.Session()
            init = tf.global_variables_initializer()
            sess.run(init)

            feed_dict = get_params_feed_dict(sess)

        feed_dict[g_p] = p

        params = [o for i, o in feed_dict.items()]
        params_controls = [Control(p) for p in params]

        feed_dict[g_data] = data

        # Run graph.
        with sess.as_default():
            output = run_tensorflow_graph(g_output, feed_dict)

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, params_controls)
        rf_both = ReducedFunction(output, [*params_controls, Control(data)])

        J = (output - 128) ** 2

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, params_controls)
        Jhat_both = ReducedFunctional(J, [*params_controls, Control(data)])

    assert output == 9

    ### Test data input.
    # Run graph using taped computation.
    assert rf_data(5) == 25

    # Do taylor test.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_data, data, h) > 1.9

    ### Test p input.
    # Run graph using taped computation.
    rf_data(2)  # Set data's block variable back to 2.
    assert rf_p([3]) == 8

    # Do taylor test.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_p, params, h) > 1.9

    ### Test data and p inputs.
    # Run graph using taped computation.
    assert rf_both([4, 3]) == 81

    # Do taylor test.
    h = [AdjFloat(1.0), AdjFloat(2.0)]
    assert taylor_test(Jhat_both, [*params, data], h) > 1.9

    # Test optimize.
    rf_data(2)  # Set data's block variable back to 2.
    p_opt = minimize(Jhat_p, callbacks=None)
    assert_allclose(p_opt, [7])

    sess.close()


if __name__ == "__main__":
    test_tensorflow_block_adjoint()
    test_tensorflow_block_jacobian_action()
    test_tensorflow_block_jacobian_matrix()
    test_tensorflow_consecutive_blocks()
    test_tensorflow_variables()
