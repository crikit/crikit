from .base import NumpyBackend
import time


class JaxNumpyBackend(NumpyBackend):
    def __init__(self):
        import jax
        from jax import numpy as jnp

        self.jax = jax
        self.seed = 0
        self.key = self._gen_key()
        super().__init__(numpy=jnp, name="jax")

    def _gen_key(self):
        return self.jax.random.PRNGKey(self.seed)

    def _next_key(self):
        self.key, self.subkey = self.jax.random.split(self.key)

    def vmap(self, f, in_axes=0, out_axes=0, axis_name=None):
        return self.jax.vmap(f, in_axes=in_axes, out_axes=out_axes, axis_name=None)

    def init_rng(self, seed=None):
        if seed is None:
            seed = int(time.time())

        self.seed = seed
        self.key = self._gen_key()

    def randn(self, *args, **kwargs):
        self._next_key()
        return self.jax.random.normal(self.subkey, shape=args, **kwargs)

    def script_if_tracing(self, f):
        return f

    def jit(self, f, *args, **kwargs):
        return self.jax.jit(f, *args, **kwargs)

    def __getattr__(self, attr, default=None):
        try:
            return getattr(self.numpy, attr)
        except Exception:
            try:
                return getattr(self.jax, attr)
            except Exception:
                return default

    def vjp(self, f, *args, **kwargs):
        return self.jax.vjp(f, *args, **kwargs)

    def index_update(self, x, idx, y):
        x.at[idx].set(y)
        return x
