def pytest_configure(config):
    # Try to import tensorflow first thing to make sure fenics is imported after it.
    try:
        import tensorflow
    except ImportError:
        pass


def pytest_runtest_setup(item):
    """ Hook function which is called before every test """
    from pyadjoint import set_working_tape
    from pyadjoint_utils import Tape

    set_working_tape(Tape())
