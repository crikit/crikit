import jax
from jax import numpy as np

from pyadjoint_utils.jax_adjoint import array
from crikit.cr import RivlinModel, ReducedFunctionJAX
from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.invariants import near
from pyadjoint_utils import taylor_test, ReducedFunction
from crikit.covering import set_default_covering_params, get_composite_cr
from crikit.cr.ufl import UFLExprSpace, UFLFunctionSpace
import numpy as onp
from pyadjoint.reduced_functional_numpy import ReducedFunctionalNumPy
from pyadjoint_utils import FileLoggerCallback


def test_rivlin_model():
    mesh = UnitSquareMesh(2, 2)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 2)
    U = TensorFunctionSpace(mesh, "P", 2)
    out_space = UFLFunctionSpace(U)
    u = project(Expression(("x[0]*x[1]", "sin(x[1])*x[1]"), degree=2), V)
    u_true = project(Expression(("x[0]*x[1]", "sin(x[1])*x[1]"), degree=2), V)
    v = TestFunction(V)
    B = dot(grad(u).T, grad(u))  # right Cauchy-Green strain -- this is
    # usually named `C` (while `B` is the left Cauchy-Green strain), but I'm
    # naming it `B` here so I can call the Rivlin model parameters `C`
    B_true = dot(grad(u_true).T, grad(u_true))
    ufl_source = UFLExprSpace(B)

    # we could use the commented-out arrays for a degree-2 Rivlin model, but
    # that takes far too long for a test that runs in the CI. It still works
    # though, so if you want to try it on your laptop, go ahead.
    C = array(np.zeros((2, 2)))  # array(np.zeros((3,3)))
    D = array(np.array([0.5, 0.25, 0.05]))

    quad_params = {"quadrature_degree": 4}
    set_default_covering_params(domain=domain, quad_params=quad_params)

    true_C = array(
        np.array([[0.0, -1.0], [0.001, 0.5]])
    )  # array(np.array([[0.0,1.0,0.5],[0.0,0.67,0.5],[0.1,2.0,2.0]]))
    true_D = array(np.array([0.5, 0.25, 0.05]))
    true_cr = get_composite_cr(
        ufl_source, RivlinModel(true_C, spatial_dims=2), out_space
    )
    true_F = (inner(true_cr(B_true), grad(v)) - inner(u_true, v)) * dx
    solve(
        true_F == 0,
        u_true,
        DirichletBC(V, Constant((0, 0)), lambda x, on_boundary: on_boundary),
    )
    rivlin = RivlinModel(C, spatial_dims=2)
    cr = get_composite_cr(ufl_source, rivlin, out_space)

    F = (inner(cr(B), grad(v)) - inner(u, v)) * dx
    solve(
        F == 0, u, DirichletBC(V, Constant((0, 0)), lambda x, on_boundary: on_boundary)
    )
    controls = [Control(C)]
    rfv = assemble(inner(u - u_true, u - u_true) * dx)
    rf = ReducedFunctional(rfv, controls)
    h = [array(onp.random.randn(*x.shape)) for x in controls]

    assert taylor_test(rf, [C], h) >= 1.9

    lower_bound = np.zeros((2, 2)) - 1.5
    upper_bound = 10 * np.ones((2, 2))
    # Truncated Newton takes about a second less time than L-BFGS-B to run on my laptop
    names = ["min_cb_1", "min_cb_2"]
    params = minimize(
        ReducedFunctionalNumPy(rf),
        method="TNC",
        callbacks=[
            FileLoggerCallback(
                names[0], filename=names[0] + ".txt", overwrite_file=True
            ),
            FileLoggerCallback(
                names[1], filename=names[1] + ".txt", overwrite_file=True
            ),
        ],
    )  #'L-BFGS-B',bounds=(lower_bound,upper_bound))
    # test callbacks
    for name in names:
        assert "Minimization Problem" in open(name + ".txt").read()

    def convert_arg(x):
        return x.value if hasattr(x, "value") else x

    rmse = np.sqrt(np.sum((true_C.value - convert_arg(params)) ** 2) / params.size)
    assert (
        rmse <= 1.0e-2
    ), f"learned params {params} are not {true_C.value}! Square root mean sum-squared difference is {rmse}!"


if __name__ == "__main__":
    test_rivlin_model()
