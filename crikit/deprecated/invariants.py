import warnings

warnings.warn(
    "This invariants implementation is deprecated. Please update your code to use the newer implementation",
    stacklevel=2,
)

from crikit.cr.autograd import point_map
from crikit.cr.numpy import Ndarrays
from crikit.cr.space_builders import DirectSum
from crikit.cr.map_builders import Callable
from crikit.cr.types import PointMap
from pyadjoint.enlisting import Enlist
from pyadjoint_utils.numpy_adjoint.autograd import overloaded_autograd
import autograd.numpy as np
from collections import namedtuple
from itertools import chain, combinations
from operator import itemgetter

TensorDefinition = namedtuple(
    "TensorDefinition", ["rank", "symmetry", "name"], defaults=(None,)
)
"""Defines tensors as needed for invariant calculations.

    The rank of the tensor is an integer. The symmetry is 's' for symmetric, 'a'
    for antisymmetric, and None for no symmetry (or for first-order tensors).
"""

RegistryKey = namedtuple("RegistryKey", ["dim", "output_def", "input_defs"])

_invariant_registry = {}


def argsort(l):
    """Returns the indices and the sorted list."""
    if len(l) == 0:
        return (), ()
    return zip(*sorted(enumerate(l), key=itemgetter(1)))


# This function could be avoided if the TensorDefinition was an actual class with a comparison operator set up to ignore the name attribute.
def remove_names(tensor_defs):
    return tuple(TensorDefinition(td.rank, td.symmetry) for td in tensor_defs)


def get_invariant_registry_key(dim, output_def, input_defs):
    """Sorts the tensor definitions."""
    canonical_input = remove_names(input_defs)
    canonical_output = remove_names((output_def,))[0]
    arg_perm, sorted_defs = argsort(canonical_input)
    return RegistryKey(dim, canonical_output, sorted_defs), arg_perm


def get_invariant_registry_entry(dim, output_def, input_defs, suppress_error=False):
    key, input_perm = get_invariant_registry_key(dim, output_def, input_defs)
    pm, registry_perm = _invariant_registry.get(key, (None, None))
    if pm is None:
        if suppress_error:
            return None, None
        raise NotImplementedError(
            "Don't have a mapping for dim %d and output tensor %s with input tensors %s"
            % (dim, output_def, input_defs)
        )
    combo_perm = tuple(input_perm[i] for i in registry_perm)
    return pm, combo_perm


def register_invariant(
    dim,
    *input_defs,
    num_invariants,
    output_def=TensorDefinition(0, None),
    invariant_point_map=None,
):
    """Register an invariant Autograd function. This can be used as a decorator.

    Args:

        dim (int): the dimension of the tensors
        *input_defs (TensorDefinition): descriptions of the input tensors
        num_invariants (int): how many invariants are returned by the function
        output_def (optional, TensorDefinition): description of the output
            tensor. The default value is a scalar.
        invariant_point_map (optional, PointMap): the point map to be registered.
            If none is given, one is automatically created using
            :func:`~crikit.cr.autograd.point_map`.

    Returns:

        If invariant_point_map is None, this returns a decorator function that
        registers the invariant. Otherwise, it returns the given point map.
    """
    if invariant_point_map is None:
        # decorator mode.
        def decorator(invariant_func):
            output_tensor_shape = output_def.rank * (dim,)
            if num_invariants == 1:
                output_shape = (-1, *output_tensor_shape)
            else:
                output_shape = (-1, num_invariants, *output_tensor_shape)
            if len(input_defs) == 0:
                # This function has no input, so it doesn't need an Autograd point map.
                pm = Callable(None, Ndarrays(output_shape), invariant_func, bare=True)
            else:
                source = build_tensor_shape(dim, input_defs)
                pm = point_map(source, output_shape, bare=True)(invariant_func)
            register_invariant(
                dim,
                *input_defs,
                num_invariants=num_invariants,
                output_def=output_def,
                invariant_point_map=pm,
            )
            return invariant_func

        return decorator
    key, inv_perm = get_invariant_registry_key(dim, output_def, input_defs)
    if key in _invariant_registry:
        warnings.warn(f"This key has already been registered")
    arg_perm, _ = argsort(inv_perm)
    _invariant_registry[key] = (invariant_point_map, arg_perm)
    return invariant_point_map


# Copied from https://docs.python.org/3/library/itertools.html
def powerset(iterable, exclude_empty_set=True):
    "powerset([1,2,3], False) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = tuple(iterable)
    return chain.from_iterable(
        combinations(s, r) for r in range(int(exclude_empty_set), len(s) + 1)
    )


def get_tensor_shape(t):
    if hasattr(t, "ufl_shape"):
        return t.ufl_shape
    # t is actually a stack of tensors, so the first dimension is simply a
    # count, not the tensor shape.
    return t.shape[1:]


def build_tensor_shape(dim, tensor_defs):
    return tuple((-1, *(d.rank * (dim,))) for d in tensor_defs)


@overloaded_autograd(True)
def concatenate_tensors(*tensors, ndim=-1):
    if ndim != -1:
        if len(tensors) > 0:
            shape = None
            for t in tensors:
                if shape is None or t.shape[0] > 1:
                    shape = t.shape
                    if len(shape) >= ndim:
                        shape = shape[:1] + shape[2:]
                    if t.shape[0] > 1:
                        break
        tensors = [
            np.expand_dims(np.broadcast_to(t, shape), axis=1) if t.ndim < ndim else t
            for t in tensors
        ]
    return np.concatenate(tensors, axis=1)


class InvariantCalculator(PointMap):
    def __init__(self, dim, output_def, input_defs):
        self._input_defs = Enlist(input_defs)
        self._maps = []
        self._perms = []
        self._names = None

        # See if there is an identity entry.
        pm, perm = get_invariant_registry_entry(
            dim, output_def, (), suppress_error=True
        )
        self._use_empty_set = pm is not None
        if self._use_empty_set:
            self._maps.append(pm)
            self._perms.append(perm)

        # Look up an invariant calculation for each subset of the given tensors.
        for subset in powerset(self._input_defs):
            pm, perm = get_invariant_registry_entry(dim, output_def, subset)
            self._maps.append(pm)
            self._perms.append(perm)

        # Create source space.
        shapes = build_tensor_shape(dim, self._input_defs)
        if len(self._input_defs) > 1:
            source = DirectSum(tuple(Ndarrays(s) for s in shapes))
        else:
            source = Ndarrays(shapes[0])

        # Check output dimensions. Each should be something like (-1, (dim,)*rank) or (-1, m, (dim,)*rank).
        output_tensor_shape = output_def.rank * (dim,)
        self.num_invariants = 0
        self.idx_invariants = [0]
        for i, pm in enumerate(self._maps):
            if not pm:
                self.idx_invariants.append(self.num_invariants)
                continue
            s = pm.target.shape()
            if len(s) == 1 + output_def.rank:
                self.num_invariants += 1
            else:
                if len(s) != 2 + output_def.rank:
                    raise ValueError(
                        f"Unexpected tensor dimensions for point map {pm} (shape = {s})."
                        + f" Should have {1+output_def.rank} or {2+output_def.rank} dimensions."
                    )
                self.num_invariants += s[1]
            s_tensor_shape = s[-output_def.rank - 1 :][1:]
            if s_tensor_shape != output_tensor_shape:
                raise ValueError(
                    f"Unexpected shape for point map {pm} (shape = {s}, tensor shape = {s_tensor_shape})."
                    + f" Expected output tensor shape to be {output_tensor_shape}"
                )
            self.idx_invariants.append(self.num_invariants)

        # Create target space.
        if self.num_invariants > 1:
            output_shape = (-1, self.num_invariants, *output_tensor_shape)
        else:
            output_shape = (-1, *output_tensor_shape)
        self._target_ndim = len(output_shape)
        target = Ndarrays(output_shape)
        super().__init__(source, target)

    def __call__(self, tensors):
        invariants = []

        # Entuple tensors beforehand.
        if not isinstance(self.source, DirectSum):
            tensors = (tensors,)

        # Calculate invariant for each subset of the given tensors.
        for subset, pm, perm in zip(
            powerset(tensors, not self._use_empty_set), self._maps, self._perms
        ):
            if not pm:
                continue
            inputs = tuple(subset[k] for k in perm)
            output = pm(inputs)
            invariants.append(pm(inputs))

        if len(invariants) == 1:
            return invariants[0]

        # Return the invariants as one numpy array.
        return concatenate_tensors(*invariants, ndim=self._target_ndim)

    def names(self):
        if self._names is not None:
            return self._names
        self._names = []
        input_names = [
            f"T_{i}({td!r})" if td.name is None else str(td.name)
            for i, td in enumerate(self._input_defs)
        ]
        for i, (subset, pm, perm) in enumerate(
            zip(powerset(input_names, not self._use_empty_set), self._maps, self._perms)
        ):
            if not pm:
                continue
            inputs = tuple(subset[k] for k in perm)
            if hasattr(pm, "_orig_func"):
                pm_name = pm._orig_func.__name__
            else:
                pm_name = pm.__name__
            start = self.idx_invariants[i]
            end = self.idx_invariants[i + 1]
            if end - start == 1:
                rang = start
            else:
                rang = ",".join(str(i) for i in range(start, end))

            name = f'{pm_name}({", ".join(inputs)}) = [{rang}]'
            self._names.append(name)
        return self._names


@overloaded_autograd(True)
def linear_combination(coefficients, basis):
    """
    Args:
        coefficients (ndarray): should have shape (n,m)
        basis (ndarray): should have shape (n, m, *)

    Returns:
        ndarray: shape (n, *)
    """
    lc = np.sum(coefficients[..., None, None] * basis, axis=1)
    return lc


def get_invariant_maps(
    dim, input_defs, output_def=TensorDefinition(0, None), group="SO"
):
    """This function returns a PointMap that computes the invariants for the
    given tensors under the given group.

    Args:
        dim (int): the dimension of the tensor spaces.
        input_defs (TensorDefinition): the definition of the input tensors.

    Returns:
        PointMap: a point map to compute the invariants for the given tensor definitions.
    """

    if group != "SO":
        raise ValueError("Can't handle any group other than SO(3) currently")

    in_pm = InvariantCalculator(dim, output_def, input_defs)

    if output_def.rank == 0:
        return in_pm

    output_tensor_shape = output_def.rank * (dim,)

    coefficient_space = Ndarrays(in_pm.target.shape()[: -output_def.rank])
    source_space = DirectSum(coefficient_space, in_pm.target)
    target_space = Ndarrays((-1, *output_tensor_shape))
    out_pm = Callable(source_space, target_space, linear_combination, bare=True)
    return in_pm, out_pm


# Define the invariants from the Zheng paper.
import crikit.deprecated.zheng
