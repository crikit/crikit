from crikit import (
    Callable,
    Parametric,
    DirectSum,
    set_default_covering_params,
    get_default_covering_params,
)
from crikit.cr import assemble_with_cr, CR, JAXArrays
from crikit.cr.ufl import CR_P_Laplacian, CR_UFL_Expr, UFLExprSpace
from crikit.invariants import TensorType
from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.cr.autograd import point_map as autograd_point_map
from crikit.cr.ufl import point_map as ufl_point_map
from pyadjoint_utils.numpy_adjoint import *
from ufl import derivative, replace

import autograd.numpy as anp
import jax
import jax.numpy as jnp
import numpy as onp
import pytest
from functools import partial
from numpy.testing import assert_allclose


@autograd_point_map(
    ((-1,), (-1, 2), (-1,), ()), (-1, 2), bare=True, pointwise=(True, True, True, False)
)
def cr_autograd_full(u, g, phi, p):
    mu = (anp.sum(g * g, axis=1) + 1e-12) ** ((p - 2.0) / 2.0)
    mu = mu * phi
    mu = anp.reshape(mu, mu.shape + (1,))
    return mu * g


def make_cr_autograd(p_init):
    def constructor(x):
        return create_overloaded_object(anp.array(x))

    p = constructor(p_init)
    return Parametric(cr_autograd_full, 3, p), p, constructor


@partial(
    overload_jax,
    argnums=(0, 1, 2, 3),
    checkpoint=True,
    jit=False,
    pointwise=(True, True, True, False),
)
@partial(jax.vmap, in_axes=(0, 0, 0, None), out_axes=0)
def jax_plap(u, g, phi, p):
    mu = jnp.sum(g * g) ** ((p - 2.0) / 2)
    return phi * mu * g


def make_cr_jax(p_init):
    def constructor(x):
        return create_overloaded_object(jnp.array(x))

    p = constructor(p_init)
    source = DirectSum(
        JAXArrays((-1,)), JAXArrays((-1, 2)), JAXArrays((-1,)), JAXArrays(())
    )
    target = JAXArrays((-1, 2))
    cr_jax_full = Callable(source, target, jax_plap, bare=True)
    return Parametric(cr_jax_full, 3, p), p, constructor


def make_cr_invariant(p_init):
    def constructor(x):
        return create_overloaded_object(jnp.array(x))

    def cr_func(invariants, p):
        phi = invariants[1]
        return phi * invariants[0:1] ** ((p - 2.0) / 2)

    p = constructor(p_init)
    input_types = (
        TensorType.make_scalar(),
        TensorType.make_vector(2),
        TensorType.make_scalar("phi"),
    )
    output_type = TensorType.make_vector(2)
    cr = CR(output_type, input_types, cr_func, vmap=True, params=(p,))
    print(cr.invariant_descriptions())
    return cr, p, constructor


@ufl_point_map(((), (2,), (), ()), bare=True)
def cr_ufl_phi_full(u, g, phi, p):
    mag2 = inner(g, g) + 1e-12
    return phi * (mag2 ** ((p - 2.0) / 2.0)) * g


@pytest.mark.parametrize("cr_maker", [make_cr_autograd, make_cr_jax, make_cr_invariant])
def test_assemble_vector(cr_maker):
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 1)
    x = SpatialCoordinate(V.mesh())
    u = project(x[0] * x[0] + x[1] * x[1], V)
    phi = project(x[0] + x[1] + 1, V)
    inputs = (u, grad(u), phi)

    pc = Constant(2.5)
    cr_ufl = Parametric(cr_ufl_phi_full, 3, pc)
    cr_numpy, p, constructor = cr_maker(float(pc))

    sigma = cr_ufl.target.point()
    v = TestFunction(V)
    form = inner(sigma, grad(v)) * dx
    vec_ufl = Function(V)
    vec_numpy = Function(V)
    assemble_with_cr(form, cr_ufl, inputs, sigma, tensor=vec_ufl)
    assemble_with_cr(form, cr_numpy, inputs, sigma, tensor=vec_numpy)

    # Assert that the UFL-native and external calculations are the same
    assert errornorm(vec_ufl, vec_numpy) < 1.0e-7

    rf_ufl = ReducedFunction(vec_ufl, Control(pc))
    vec_ufl2 = rf_ufl(3.0)
    rf_numpy = ReducedFunction(vec_numpy, Control(p))
    vec_numpy2 = rf_numpy(3.0)

    # Assert that pyadjoint taped both functions identically
    assert errornorm(vec_ufl2, vec_numpy2) < 1.0e-7

    # Run taylor tests with adjoint.
    hc = Constant(1e-1)
    h = constructor(float(hc))

    v_vec = Function(V)
    v_vec.vector()[:] = onp.random.rand(len(v_vec.vector()))
    new_pc = Constant(3.0)
    new_p = constructor(float(new_pc))
    assert taylor_test(rf_ufl, new_pc, v=v_vec.vector(), h=hc) > 1.9
    assert taylor_test(rf_numpy, new_p, v=v_vec.vector(), h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, new_pc, h=hc) > 1.9
    assert taylor_test(rf_numpy, new_p, h=h) > 1.9

    # Similar tests but with u as the control.
    x = SpatialCoordinate(V.mesh())
    u_new = project(
        2 * x[0] * x[0] * x[1] + 2 * x[1] * x[1] * x[0] + 0.01 * (x[0] + x[1]), V
    )

    rf_ufl = ReducedFunction(vec_ufl, Control(u))
    vec_ufl2 = rf_ufl(u_new)
    rf_numpy = ReducedFunction(vec_numpy, Control(u))
    vec_numpy2 = rf_numpy(u_new)

    assert errornorm(vec_ufl2, vec_numpy2) < 1.0e-7

    # Run taylor tests with adjoint.
    h = Function(V)
    h.vector()[:] = 0.1 * onp.random.rand(V.dim())

    assert taylor_test(rf_ufl, u_new, v=v_vec.vector(), h=h) > 1.9
    assert taylor_test(rf_numpy, u_new, v=v_vec.vector(), h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, u_new, h=h) > 1.9
    assert taylor_test(rf_numpy, u_new, h=h) > 1.9

    # Test Jacobian.
    rfnp = ReducedFunctionNumPy(rf_numpy)
    J_numpy = rfnp.jac_matrix().array()

    u_hat = TrialFunction(V)
    cr_ufl.set_param_point(Constant(3.0))
    cr_form = replace(form, {sigma: cr_ufl(inputs)})
    der_ufl = derivative(replace(cr_form, {u: u_new}), u_new, u_hat)
    J_ufl = assemble(der_ufl).array()
    assert_allclose(J_numpy, J_ufl)


@autograd_point_map(
    ((-1,), (-1, 2), (-1,), ()), (-1,), bare=True, pointwise=(True, True, True, False)
)
def cr_autograd_scalar_full(u, g, phi, p):
    mu = (anp.sum(g * g, axis=1)) ** ((p - 2.0) / 2.0)
    mu = mu * phi
    return mu


def make_cr_autograd_scalar(p_init):
    def constructor(x):
        return create_overloaded_object(anp.array(x))

    p = constructor(p_init)
    return Parametric(cr_autograd_scalar_full, 3, p), p, constructor


@partial(
    overload_jax,
    argnums=(0, 1, 2, 3),
    checkpoint=True,
    jit=False,
    pointwise=(True, True, True, False),
)
@partial(jax.vmap, in_axes=(0, 0, 0, None), out_axes=0)
def cr_jax_scalar(u, g, phi, p):
    mu = jnp.sum(g * g) ** ((p - 2.0) / 2)
    return phi * mu


def make_cr_jax_scalar(p_init):
    def constructor(x):
        return create_overloaded_object(jnp.array(x))

    p = constructor(p_init)
    source = DirectSum(
        JAXArrays((-1,)), JAXArrays((-1, 2)), JAXArrays((-1,)), JAXArrays(())
    )
    target = JAXArrays((-1,))
    cr_jax_full = Callable(source, target, cr_jax_scalar, bare=True)
    return Parametric(cr_jax_full, 3, p), p, constructor


def make_cr_invariant_scalar(p_init):
    def constructor(x):
        return create_overloaded_object(jnp.array(x))

    def cr_func(invariants, p):
        phi = invariants[1]
        return phi * invariants[0:1] ** ((p - 2.0) / 2)

    p = constructor(p_init)
    input_types = (
        TensorType.make_scalar(),
        TensorType.make_vector(2),
        TensorType.make_scalar("phi"),
    )
    output_type = TensorType.make_scalar()
    cr = CR(output_type, input_types, cr_func, vmap=True, params=(p,))
    print(cr.invariant_descriptions())
    return cr, p, constructor


@ufl_point_map(((), (2,), (), ()), bare=True)
def cr_ufl_scalar_full(u, g, phi, p):
    mag2 = inner(g, g) + 1e-12
    return phi * (mag2 ** ((p - 2.0) / 2.0))


@pytest.mark.parametrize(
    "cr_maker", [make_cr_autograd_scalar, make_cr_jax_scalar, make_cr_invariant_scalar]
)
def test_scalar_cr(cr_maker):
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 1)
    x = SpatialCoordinate(V.mesh())
    u = project(x[0] * x[0] + x[1] * x[1], V)
    phi = project(1 + x[0] + x[1], V)
    inputs = (u, grad(u), phi)

    pc = Constant(2.5)
    cr_ufl = Parametric(cr_ufl_scalar_full, 3, pc)
    cr_numpy, p, constructor = cr_maker(float(pc))

    sigma = cr_ufl.target.point()
    v = TestFunction(V)
    form = inner(sigma, v) * dx
    vec_ufl = Function(V)
    vec_numpy = Function(V)
    assemble_with_cr(form, cr_ufl, inputs, sigma, tensor=vec_ufl)
    assemble_with_cr(form, cr_numpy, inputs, sigma, tensor=vec_numpy)

    # Assert that the UFL-native and external calculations are the same
    assert errornorm(vec_ufl, vec_numpy) < 1.0e-7

    rf_ufl = ReducedFunction(vec_ufl, Control(pc))
    vec_ufl2 = rf_ufl(3.0)
    rf_numpy = ReducedFunction(vec_numpy, Control(p))
    vec_numpy2 = rf_numpy(3.0)

    # Assert that pyadjoint taped both functions identically
    assert errornorm(vec_ufl2, vec_numpy2) < 1.0e-7

    # Run taylor tests with adjoint.
    hc = Constant(1e-1)
    h = constructor(float(hc))

    v_vec = Function(V)
    v_vec.vector()[:] = onp.random.rand(len(v_vec.vector()))
    new_pc = Constant(3.0)
    new_p = constructor(float(new_pc))
    assert taylor_test(rf_ufl, new_pc, v=v_vec.vector(), h=hc) > 1.9
    assert taylor_test(rf_numpy, new_p, v=v_vec.vector(), h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, new_pc, h=hc) > 1.9
    assert taylor_test(rf_numpy, new_p, h=h) > 1.9

    # Similar tests but with u as the control.
    x = SpatialCoordinate(V.mesh())
    u_new = project(
        2 * x[0] * x[0] * x[1] + 2 * x[1] * x[1] * x[0] + 0.01 * (x[0] + x[1]), V
    )

    rf_ufl = ReducedFunction(vec_ufl, Control(u))
    vec_ufl2 = rf_ufl(u_new)
    rf_numpy = ReducedFunction(vec_numpy, Control(u))
    vec_numpy2 = rf_numpy(u_new)

    assert errornorm(vec_ufl2, vec_numpy2) < 1.0e-7

    # Run taylor tests with adjoint.
    h = Function(V)
    h.vector()[:] = 0.1 * onp.random.rand(V.dim())

    assert taylor_test(rf_ufl, u_new, v=v_vec.vector(), h=h) > 1.9
    assert taylor_test(rf_numpy, u_new, v=v_vec.vector(), h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, u_new, h=h) > 1.9
    assert taylor_test(rf_numpy, u_new, h=h) > 1.9

    # Test Jacobian.
    rfnp = ReducedFunctionNumPy(rf_numpy)
    J_numpy = rfnp.jac_matrix().array()

    u_hat = TrialFunction(V)
    cr_ufl.set_param_point(Constant(3.0))
    cr_form = replace(form, {sigma: cr_ufl(inputs)})
    der_ufl = derivative(replace(cr_form, {u: u_new}), u_new, u_hat)
    J_ufl = assemble(der_ufl).array()
    assert_allclose(J_numpy, J_ufl)


@autograd_point_map(
    ((-1,), (-1, 2), (-1,), ()), (-1,), bare=True, pointwise=(True, True, True, False)
)
def cr_autograd_energy(u, g, phi, p):
    mag2 = anp.sum(g * g, axis=1) + 1e-12
    energy = phi * (mag2 ** (p / 2.0)) / p
    return energy


@ufl_point_map(((), (2,), (), ()), bare=True)
def cr_ufl_energy_full(u, g, phi, p):
    mag2 = inner(g, g) + 1e-12
    return phi * (mag2 ** (p / 2.0)) / p


def test_assemble_scalar():
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 1)
    x = SpatialCoordinate(V.mesh())
    u = project(x[0] * x[0] + x[1] * x[1], V)
    phi = project(x[0] + x[1] + 1, V)
    inputs = (u, grad(u), phi)

    p = create_overloaded_object(anp.array(2.5))
    pc = Constant(p)

    cr_ufl = Parametric(cr_ufl_energy_full, 3, pc)
    cr_numpy = Parametric(cr_autograd_energy, 3, p)

    sigma = cr_ufl.target.point()
    form = sigma * dx(domain=mesh.ufl_domain())
    e_ufl = assemble_with_cr(form, cr_ufl, inputs, sigma)
    e_numpy = assemble_with_cr(form, cr_numpy, inputs, sigma)

    # Assert that the UFL-native and external calculations are the same
    assert_allclose(e_ufl, e_numpy)

    # Test that pyadjoint taped both functions identically
    rf_ufl = ReducedFunction(e_ufl, Control(pc))
    e_ufl2 = rf_ufl(3.0)
    rf_numpy = ReducedFunction(e_numpy, Control(p))
    e_numpy2 = rf_numpy(3.0)

    assert_allclose(e_ufl2, e_numpy2)

    # Run taylor tests with adjoint.
    h = create_overloaded_object(anp.array(1e-1))
    hc = Constant(h)
    v = AdjFloat(2.0)

    assert taylor_test(rf_ufl, Constant(3.0), v=v, h=hc) > 1.9
    assert taylor_test(rf_numpy, anp.array(3.0), v=v, h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, Constant(3.0), h=hc) > 1.9
    assert taylor_test(rf_numpy, anp.array(3.0), h=h) > 1.9


@pytest.mark.parametrize("cr_maker", [make_cr_autograd, make_cr_jax, make_cr_invariant])
def test_solve(cr_maker):
    mesh = UnitSquareMesh(4, 4)
    V = FunctionSpace(mesh, "P", 1)
    x = SpatialCoordinate(V.mesh())

    pc = Constant(3.5)
    cr_ufl = Parametric(cr_ufl_phi_full, 3, pc)
    cr_numpy, p, constructor = cr_maker(float(pc))

    u = project(x[0] * x[0] + x[1] * x[1], V)
    phi = project(10 * (x[0] + x[1]) + 1, V)
    inputs = (u, grad(u), phi)
    v = TestFunction(V)

    # Test with a non-symmetric system.
    sigma = cr_ufl.target.point()
    form = (u * inner(sigma, grad(v)) - inner(v, Constant(1))) * dx

    # Test with a non-homogeneous boundary condition.
    def bottom_left_boundary(x, on_boundary):
        return on_boundary and (near(x[0], 0, 1e-5) or near(x[1], 0, 1e-5))

    bc = DirichletBC(V, Constant(0.5), bottom_left_boundary)

    # Solve with standard solve.
    u_ufl = u.copy(deepcopy=True)
    sigma_ufl = cr_ufl(inputs)
    form_ufl = replace(form, {sigma: sigma_ufl})
    form_ufl = replace(form_ufl, {u: u_ufl})
    solve(form_ufl == 0, u_ufl, bc)

    # Solve with SNESSolver.
    with push_tape():
        residual = Function(V)
        ucontrol = Control(u)
        assemble_with_cr(form, cr_numpy, inputs, sigma, tensor=residual)
        residual_rf = ReducedFunction(residual, ucontrol)

    reduced_equation = ReducedEquation(residual_rf, bc, homogenize_bcs(bc))
    solver = SNESSolver(reduced_equation, dict(jmat_type="assembled"))
    u = solver.solve(ucontrol)

    assert errornorm(u, u_ufl) < 1.0e-7

    def functional(u):
        return assemble(u * u * dx)

    # Test Jacobian.
    uhat = TrialFunction(V)
    dFdu_ufl = assemble(derivative(form_ufl, u_ufl, uhat)).array()
    dFdu = residual_rf.jac_matrix().array()
    assert_allclose(dFdu_ufl, dFdu, atol=1e-10)

    # Test derivative.
    J_ufl = functional(u_ufl)
    Jhat_ufl = ReducedFunctional(J_ufl, Control(pc))
    dJdm_ufl = Jhat_ufl.derivative()

    J = functional(u)
    Jhat = ReducedFunctional(J, Control(p))
    dJdm = Jhat.derivative()

    assert_allclose(J_ufl, J)
    assert_allclose(float(dJdm_ufl), float(dJdm))
