# from crikit.jax_network import Layer, Network
import jax
from jax import numpy as np
import numpy as onp
from pyadjoint import Control
from pyadjoint_utils.verification import taylor_test
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint_utils import AdjFloat, ReducedFunction, taylor_test, ndarray, array
from crikit.cr import CR, ReducedFunctionJAX, cr_function_shape
from crikit.cr.ufl import UFLExprSpace, UFLFunctionSpace, CR_UFL_Expr
from crikit.covering import get_composite_cr, set_default_covering_params
from crikit.invariants import TensorType
from jax.experimental.stax import Dense, Relu, FanOut, Tanh, LogSoftmax, FanInSum
from jax.experimental import stax
from jax.experimental import optimizers
from jax.tree_util import tree_flatten, tree_unflatten
from crikit.projection import project
import pytest

saved_sigma = None
eps = None
params = None
source = None
out = None


def test_network():
    mesh = UnitSquareMesh(3, 3)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 2)
    U = TensorFunctionSpace(mesh, "P", 2)
    out_space = UFLFunctionSpace(U)
    u = project(Expression(("x[0]*x[1]", "sin(x[1])*x[1]"), degree=2), V)
    v = TestFunction(V)
    B = dot(grad(u), grad(u).T)  # left Cauchy-Green strain
    ufl_source = UFLExprSpace(B)
    global source
    source = ufl_source
    global out
    out = out_space
    input_type = TensorType.make_symmetric(2, 2)
    output_type = TensorType.make_symmetric(2, 2)
    num_scalar_invts, num_scalar_funcs = cr_function_shape(output_type, (input_type,))
    # network based on encoder from https://github.com/google/jax/blob/master/examples/mnist_vae.py
    # not a serious attempt at good neural network design, just a test that things work with a
    # non-trivial network
    init_random_params, predict = stax.serial(
        Dense(num_scalar_invts),
        Relu,
        Dense(2),
        Tanh,
        Dense(num_scalar_funcs),
        Tanh,
    )
    rng = jax.random.PRNGKey(0)
    step = 0.01
    mass = 0.9
    opt_init, opt_update, get_params = optimizers.momentum(step, mass=mass)
    _, init_params = init_random_params(rng, (-1, num_scalar_funcs))
    iflat, itreedef = tree_flatten(init_params)
    init_params = tree_unflatten(
        itreedef, [np.asarray(x, dtype=np.float64) for x in iflat]
    )
    flats, treedef = tree_flatten(init_params)
    jax_params = [array(x) for x in flats]
    jax_treedef, jax_flats = tree_flatten(jax_params)
    # make sure everything is a float64 by setting the init_params to be values derived from crikit,
    # which uses float64 as its default data type, as opposed to jax, which uses float32
    state = opt_init(tree_unflatten(treedef, [x.value for x in jax_params]))

    def model(invts, *flat_params):

        return predict(tree_unflatten(treedef, list(flat_params)), invts)

    cr = CR(output_type, (input_type,), model, vmap=True, params=jax_params)
    quad_params = {"quadrature_degree": 4}

    set_default_covering_params(domain=domain, quad_params=quad_params)

    comp_cr = get_composite_cr(ufl_source, cr, out_space)
    # define and solve the nonlinear PDE
    sigma = comp_cr(B)
    F = (inner(sigma, grad(v)) - inner(u, v)) * dx
    solve(
        F == 0, u, DirichletBC(V, Constant((0, 0)), lambda x, on_boundary: on_boundary)
    )
    rfv = assemble(inner(u, u) * dx)
    rf = ReducedFunction(rfv, [Control(x) for x in jax_params])
    rfj = ReducedFunctionJAX(rf)
    h = [0.01 * array(onp.random.randn(*x.shape)) for x in jax_params]
    res = taylor_test(rfj, jax_params, h)
    # higher tolerance than usual, but this is a pretty nonlinear function
    assert res >= 1.8
    try:
        cr.save_model(".")
        global eps
        eps = B
        global saved_sigma
        saved_sigma = sigma
        global params
        params = init_params
    except:
        # if tensorflow isn't installed, this fails
        pass


@pytest.mark.skip(
    reason="The loaded TensorFlow model is expecting numpy arrays, not JAX tracers, and that causes problems in vmap"
)
def test_restore_cr():
    input_type = TensorType.make_symmetric(2, 2)
    output_type = TensorType.make_symmetric(2, 2)
    restored_cr = CR(
        output_type, (input_type,), ".", vmap=True, params=params, nojit=True
    )
    comp_cr = get_composite_cr(source, restored_cr, out)
    rsigma = comp_cr(eps)
    assert errornorm(saved_sigma, rsigma) <= 1.0e-6


if __name__ == "__main__":
    test_network()
