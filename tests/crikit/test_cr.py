import pytest
from crikit.cr.types import PointMap
from pyadjoint_utils import AdjFloat


class CRExample(PointMap):
    def __init__(self, a):
        super(CRExample, self).__init__(((3,),), ((),))
        self.a = a

    def __call__(self, x):
        return ((x[0] * x[0] + x[1] * x[1] + x[2] * x[2]) ** self.a,)


@pytest.mark.skip(reason="haven't done bare vs not-bare stuff yet")
def test_cr_apply():

    cr = CRExample(AdjFloat(3.0))

    v = (AdjFloat(0.0), AdjFloat(1.0), AdjFloat(2.0))

    y = cr.apply(v)[0]
    y2 = cr(v[0], v[1], v[2])

    assert isinstance(y, AdjFloat)
    assert y == y2
    assert y == 125
