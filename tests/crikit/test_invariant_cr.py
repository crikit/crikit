from crikit import (
    CR,
    get_invariant_functions,
    get_invariant_descriptions,
    register_invariant_functions,
    InvariantInfo,
    TensorType,
    LeviCivitaType,
    levi_civita,
    near,
    get_default_backend,
    symm,
    cr_function_shape,
)
from crikit.invariants.utils import _3d_rotation_matrix
from pyadjoint_utils import *
from pyadjoint_utils.jax_adjoint.array import convert_arg
from pyadjoint_utils.numpy_adjoint import ndarray
import jax
from jax import numpy as np
from jax import random
from functools import partial
import torch
import numpy as onp
from numpy.testing import assert_allclose


def sym(x):
    return x + x.T


def _2d_rotation_matrix(theta):
    return np.array([[np.cos(theta), np.sin(theta)], [-np.sin(theta), np.cos(theta)]])


def rotation_rank_2_action(x, g):
    return g.T @ x @ g


def test_description():
    inputs = (
        TensorType.make_symmetric(2, 3, "epsilon"),
        TensorType.make_symmetric(2, 3),
        TensorType.make_vector(3, "k"),
        TensorType.make_symmetric(2, 3),
        TensorType.make_vector(3),
        TensorType.make_scalar("P"),
    )
    output = TensorType.make_symmetric(2, 3)
    info = InvariantInfo(2, inputs, output)
    print(get_invariant_descriptions(info, suppress_warning_print=True))


def test_3d_two_rank_two():
    inputs = (TensorType.make_symmetric(2, 3), TensorType.make_symmetric(2, 3))
    info = InvariantInfo(3, inputs, TensorType.make_symmetric(2, 3))
    print(get_invariant_descriptions(info, suppress_warning_print=False))
    s_inv, f_inv = get_invariant_functions(info, fail_on_warning=False)

    one = np.diag(np.array([2, 3, 5]))
    two = np.diag(np.array([7, 11, 13]))

    scalars = s_inv(one, two)

    def get_scalars(A, B):
        A2 = A @ A
        B2 = B @ B
        a = [np.trace(A), np.trace(A2), np.trace(A @ A2)]
        b = [np.trace(B), np.trace(B2), np.trace(B @ B2)]
        ab = [np.trace(A @ B), np.trace(A2 @ B), np.trace(A @ B2), np.trace(A2 @ B2)]
        return np.array(a + b + ab)

    correct = get_scalars(one, two)

    # print('correct', 'invariants.py')
    for m, s in zip(correct, scalars):
        assert m == s


def test_2d_isotropic_one_rank_2():

    example_inputs = (np.eye(2),)
    example_output = np.eye(2)
    # one symmetric rank-2 input, one symmetric rank-2 output
    def cr_func(scalar_invts, p):
        return scalar_invts ** ((p - 2.0) / 2.0)

    p = array(np.array([2.5]))
    cr = CR.from_arrays(
        example_output,
        example_inputs,
        cr_func,
        params=(p,),
        vmap=False,
    )

    example_eps = array(sym(np.array([[3.0, 4.0], [5.0, 6.0]])))
    example_sigma = cr(example_eps)

    thetas = [0.1, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(example_eps.value)
    for th in thetas:
        rotated_input = array(
            rotation_rank_2_action(example_eps.value, _2d_rotation_matrix(th))
        )
        rotated_output = cr(rotated_input)
        assert np.allclose(sinvts, cr.scalar_invariants(rotated_input.value))
        rotated_output = rotation_rank_2_action(
            rotated_output.value, _2d_rotation_matrix(-th)
        )
        assert np.allclose(rotated_output, example_sigma.value)


def test_2d_hemitropic_one_rank_2():

    eps_ij = levi_civita(2, get_default_backend())
    example_inputs = (np.eye(2), eps_ij)
    example_output = np.eye(2)
    # one symmetric rank-2 input, one symmetric rank-2 output
    def cr_func(scalar_invts, p):
        return np.array([scalar_invts[0], scalar_invts[1], np.mean(scalar_invts)]) ** (
            (p - 2.0) / 2.0
        )

    p = array(np.array([2.5]))
    cr = CR.from_arrays(example_output, example_inputs, cr_func, (p,), vmap=False)

    example_eps = array(sym(np.array([[3.0, 4.0], [5.0, 6.0]])))
    example_sigma = cr(example_eps)

    thetas = [0.1, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(example_eps.value)
    for th in thetas:
        rotated_input = array(
            rotation_rank_2_action(example_eps.value, _2d_rotation_matrix(th))
        )
        rotated_output = cr(rotated_input)
        assert np.allclose(sinvts, cr.scalar_invariants(rotated_input.value))
        rotated_output = rotation_rank_2_action(
            rotated_output.value, _2d_rotation_matrix(-th)
        )
        assert np.allclose(rotated_output, example_sigma.value)

    @partial(overload_jax, argnums=(1,))
    def of(x, p):
        cr.set_params((p,))
        return np.sum(cr(x))

    v = of(example_eps, p)

    rf = ReducedFunction(v, Control(p))

    h = tuple(1.0e-2 * array(onp.random.randn(*x.shape)) for x in (p,))
    p = array([2.8])
    res = taylor_test(rf, p, h)

    assert res >= 1.9


def test_3d_transverse_isotropy():

    example_inputs = (np.eye(3), np.ones((3,)))
    example_output = np.eye(3)

    def cr_func(scalar_invts, p):
        return ((scalar_invts + 1.0e-12) ** p)[0:8]

    p = array(np.array([2.5]))
    cr = CR.from_arrays(example_output, example_inputs, cr_func, (p,), vmap=False)

    example_eps = array(np.eye(3))
    example_v = array(np.array([1.0, 1.0, 1.0]) / np.sqrt(3))
    example_sigma = cr((example_eps, example_v))

    thetas = [-0.05, 0.1, 0.3, 0.5, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(example_eps.value, example_v.value)

    for th in thetas:
        rotmat = _3d_rotation_matrix(
            np.array([1.0, 1.0, 1.0]) / np.sqrt(3), th
        )  # rotation about the vector that is the structural tensor
        rotated_eps = array(rotmat @ example_eps.value @ rotmat.T)
        rotated_v = array(rotmat @ example_v.value)
        rotated_sigma = cr((rotated_eps, rotated_v))
        rsi = cr.scalar_invariants(rotated_eps.value, rotated_v.value)
        irotmat = _3d_rotation_matrix(example_v.value, -th)
        assert np.allclose(rsi, sinvts, rtol=1.0e-1), f"theta = {th}, {rsi} != {sinvts}"

    si, fi = cr._scalar_invt_func, cr._form_invt_func
    f = cr._f
    eval_invt_cr = cr._invariant_evaluator

    @partial(overload_jax, argnums=(0, 1, 2))
    def cr_fun(p, eps, v):
        sinvts = si(eps, v)
        finvts = fi(eps, v)
        phi_i = f(sinvts, p)
        return eval_invt_cr(phi_i, finvts)

    # ensure that everything is differentiable
    @partial(overload_jax, argnums=(2,))
    def cr_class_fun(eps, v, p):
        cr.set_params((p,))
        return cr((eps, v))

    sigma = cr_fun(p, example_eps, example_v)
    rf = ReducedFunction(sigma, [Control(p), Control(example_eps), Control(example_v)])
    ssigma = cr_class_fun(example_eps, example_v, p)
    rrf = ReducedFunction(ssigma, Control(p))
    hp = (1.0e-2 * array(onp.random.randn(*p.shape)),)
    h = tuple(
        1.0e-2 * array(onp.random.randn(*x.shape))
        for x in (p, example_sigma, example_v)
    )
    res = taylor_test(rrf, (p,), hp)
    assert res >= 1.9
    res = taylor_test(rf, (p, example_eps, example_v), h)
    assert res >= 1.9


def test_3d_isotropy_temperature():
    eps = array(np.eye(3))
    T = array(np.array([0.5]))
    ex_out = np.eye(3)
    n_scalar, n_phi = cr_function_shape(ex_out, (eps, T))

    def cr_func(scalar_invts, p):
        return np.mean(scalar_invts) ** ((p - 2.0) / 2.0) * np.ones((n_phi,))

    p = array(2.5)
    cr = CR.from_arrays(
        ex_out,
        (
            eps,
            T,
        ),
        cr_func,
        params=(p,),
        vmap=False,
    )
    thetas = [-0.05, 0.1, 0.3, 0.5, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(eps.value, T.value)
    ex_sigma = cr((eps, T))
    for th in thetas:
        ax = onp.random.randn(3)
        ax = ax / onp.linalg.norm(ax)
        rotmat = _3d_rotation_matrix(ax, th)
        rotated_eps = rotmat @ eps.value @ rotmat.T
        rsi = cr.scalar_invariants(rotated_eps, T.value)
        assert np.allclose(rsi, sinvts, rtol=1.0e-1), f"theta = {th}, {rsi} != {sinvts}"

    rf = ReducedFunction(ex_sigma, Control(p))
    h = tuple(1.0e-1 * array(onp.random.randn(*x.shape)) for x in (p,))
    assert taylor_test(rf, (p,), h) >= 1.9


def test_2d_isotropic_one_rank_2_replacement():
    def test_invariants(A, backend):
        return backend.linalg.eig(A)[
            0
        ]  # np.array([np.sum(np.sum(A, axis=0), axis=0), 1])

    def test_equivariants(A, backend):
        vs = backend.linalg.eig(A)[1]
        return [backend.tensordot(vs[:, 0], vs[:, 0], axes=0)]

    info = InvariantInfo(
        2, (TensorType.make_symmetric(2, 2),), TensorType.make_symmetric(2, 2)
    )
    # so we don't permanently overwrite these
    def old_scalars(A, backend):
        return backend.concatenate(
            list(map(backend.atleast_1d, [backend.trace(A), backend.trace(A @ A)]))
        )

    def old_equivariants(A, backend):
        return [A]

    register_invariant_functions(
        info,
        test_invariants,
        test_equivariants,
        overwrite_existing=True,
    )
    example_inputs = (np.eye(2),)
    example_output = np.eye(2)
    # one symmetric rank-2 input, one symmetric rank-2 output
    def cr_func(scalar_invts, p):
        return scalar_invts ** (p - 2) / 2

    p = array(np.array([2.5]))
    cr = CR.from_arrays(
        example_output,
        example_inputs,
        cr_func,
        params=(p,),
        vmap=False,
    )

    example_eps = array(sym(np.array([[3.0, 4.0], [5.0, 6.0]])))
    example_sigma = cr(example_eps)

    # we can replace with the old ones now that the CR has been constructed
    register_invariant_functions(
        info,
        old_scalars,
        old_equivariants,
        overwrite_existing=True,
    )


def test_2d_torch():

    input_types = (TensorType.make_symmetric(2, 2),)
    output_type = TensorType.make_symmetric(2, 2)

    def cr_func(scalar_invts, p):
        return scalar_invts ** ((p - 2.0) / 2.0)

    def cr_func_torch(scalar_invts, p):
        return scalar_invts ** ((p - 2.0) / 2.0)

    sym = lambda x: x + x.T
    p_jax = array([2.5])
    p_torch = tensor([2.5])
    cr_jax = CR(
        output_type, input_types, cr_func, params=(p_jax,), vmap=False, backend="jax"
    )

    example_eps_jax = array(sym(np.array([[3.0, 4.0], [5.0, 6.0]])))
    example_sigma = cr_jax(example_eps_jax)
    example_eps_torch = tensor(
        torch.as_tensor(sym(onp.array([[3.0, 4.0], [5.0, 6.0]])))
    )
    cr_torch = CR(
        output_type,
        input_types,
        cr_func_torch,
        params=(p_torch,),
        vmap=False,
        backend="torch",
    )
    example_sig_torch = cr_torch(example_eps_torch)

    torch_sigma = example_sig_torch.detach().numpy()
    assert np.allclose(torch_sigma, example_sigma.unwrap(True))


def test_2d_torch_vmap():

    input_types = (TensorType.make_symmetric(2, 2),)
    output_type = TensorType.make_symmetric(2, 2)

    def cr_func(scalar_invts, p):
        return np.sin(scalar_invts) * p

    def cr_func_torch(scalar_invts, p):
        return torch.sin(scalar_invts) * p

    sym = lambda x: x + x.T
    p_jax = array([2.5])
    p_torch = tensor([2.5])
    cr_jax = CR(output_type, input_types, cr_func, params=(p_jax,), backend="jax")

    example_eps_jax = array(onp.random.randn(50, 2, 2))
    example_sigma = cr_jax(example_eps_jax)
    example_eps_torch = tensor(example_eps_jax.unwrap())
    cr_torch = CR(
        output_type,
        input_types,
        cr_func_torch,
        params=(p_torch,),
        backend="torch",
    )
    example_sig_torch = cr_torch(example_eps_torch)

    torch_sigma = example_sig_torch.detach().numpy()
    assert np.allclose(torch_sigma, example_sigma.unwrap(True))


def test_3d_torch():

    input_types = (
        TensorType.make_symmetric(2, 3),
        TensorType.make_symmetric(2, 3),
        TensorType.make_vector(3),
        LeviCivitaType(3),
    )
    output_type = TensorType.make_symmetric(2, 3)

    def cr_func(scalar_invts, p):
        return scalar_invts[0:15] ** ((p - 2.0) / 2.0)

    def cr_func_torch(scalar_invts, p):
        return scalar_invts[0:15] ** ((p - 2.0) / 2.0)

    sym = lambda x: x + x.T
    p_jax = array([2.5])
    p_torch = tensor([2.5])
    cr_jax = CR(
        output_type, input_types, cr_func, params=(p_jax,), vmap=False, backend="jax"
    )

    example_eps_jax = array(
        sym(np.array([[3.0, 4.0, 5.0], [5.0, 6.0, 7.0], [7.0, 8.0, 9.0]]))
    )
    jax_inputs = [example_eps_jax] * 2 + [array([1.0, 2.0, 3.0])]
    example_sigma = cr_jax(tuple(jax_inputs))
    example_eps_torch = tensor(
        torch.as_tensor(
            sym(onp.array([[3.0, 4.0, 5.0], [5.0, 6.0, 7.0], [7.0, 8.0, 9.0]]))
        )
    )
    torch_inputs = [example_eps_torch] * 2 + [tensor([1.0, 2.0, 3.0])]
    cr_torch = CR(
        output_type,
        input_types,
        cr_func_torch,
        params=(p_torch,),
        vmap=False,
        backend="torch",
    )
    example_sig_torch = cr_torch(tuple(torch_inputs))

    torch_sigma = example_sig_torch.detach().numpy()
    assert np.allclose(torch_sigma, example_sigma.unwrap(True))

    example_eps_jax = array(
        sym(np.array([[5.0, 46.0, 7.0], [5.0, 6.0, 7.0], [17.0, 68.0, 0.9]]))
    )
    jax_inputs = [example_eps_jax] * 2 + [array([2.0, 1.0, 3.0])]
    example_sigma = cr_jax(tuple(jax_inputs))
    example_eps_torch = tensor(
        torch.as_tensor(
            sym(onp.array([[5.0, 46.0, 7.0], [5.0, 6.0, 7.0], [17.0, 68.0, 0.9]]))
        )
    )
    torch_inputs = [example_eps_torch] * 2 + [tensor([2.0, 1.0, 3.0])]
    example_sig_torch = cr_torch(tuple(torch_inputs))

    torch_sigma = example_sig_torch.detach().numpy()
    assert np.allclose(torch_sigma, example_sigma.unwrap(True))


def test_2d_torch_saved_cr(filename="torch_cr.onnx"):

    input_types = (TensorType.make_symmetric(2, 2),)
    output_type = TensorType.make_symmetric(2, 2)

    def cr_func(scalar_invts, p):
        return torch.sin(scalar_invts) * p

    sym = lambda x: x + x.T
    p_torch = tensor([2.5])
    cr_torch = CR(
        output_type,
        input_types,
        cr_func,
        params=(p_torch,),
        backend="torch",
        save_onnx_to=filename,
    )

    example_eps_torch = tensor(onp.random.randn(50, 2, 2))
    example_sig_torch = cr_torch(example_eps_torch)
    cr_onnx = CR(
        output_type,
        input_types,
        filename,
        params=(p_torch,),
        backend="jax",
        jit=False,
        vmap_inner=False,
    )

    onnx_sigma = cr_onnx(ndarray._ad_init_object(example_eps_torch.detach().numpy()))
    torch_sigma = example_sig_torch.detach().numpy()
    assert onp.allclose(onnx_sigma, torch_sigma)


def test_cr_point_maps():
    # Test an isotropic function.
    input_types = (TensorType.make_symmetric(2, 2),)
    output_type = TensorType.make_symmetric(2, 2)

    def cr_func(scalar_invts, p):
        return scalar_invts ** ((p - 2.0) / 2.0)

    inputs = array(sym(np.array([[3.0, 4.0], [5.0, 6.0]])))
    p = array(np.array([2.5]))
    cr = CR(
        output_type,
        input_types,
        cr_func,
        params=(p,),
        vmap=False,
    )
    invariant_maps_assertions(cr, cr_func, inputs, p)

    # Test an isotropic v-mapped function.
    a = onp.random.randn(5, 2, 2)
    inputs = array(a + onp.transpose(a, (0, 2, 1)))
    cr = CR(
        output_type,
        input_types,
        cr_func,
        params=(p,),
        vmap=True,
    )
    invariant_maps_assertions(cr, cr_func, inputs, p)

    # Test a transversly isotropic v-mapped function.
    def cr_func(scalar_invts, p):
        return scalar_invts[1:] ** ((p - 2.0) / 2.0)

    input_types = (TensorType.make_symmetric(2, 2), TensorType.make_vector(2))
    a = onp.random.randn(5, 2, 2)
    b = onp.random.randn(5, 2)
    inputs = (array(a + onp.transpose(a, (0, 2, 1))), array(b))
    cr = CR(
        output_type,
        input_types,
        cr_func,
        params=(p,),
        vmap=True,
    )
    invariant_maps_assertions(cr, cr_func, inputs, p)


def invariant_maps_assertions(cr, cr_func, inputs, p):
    inputs_enlist = Enlist(inputs)
    converted_inputs = convert_arg(inputs_enlist)
    scalar_invt_map, form_invt_map, inner_map, coeff_form_map = cr.get_point_maps()

    scalar_invts = scalar_invt_map(inputs)
    assert_allclose(scalar_invts, cr._scalar_invt_func(*converted_inputs))

    form_invts = form_invt_map(inputs)
    assert_allclose(form_invts, cr._form_invt_func(*converted_inputs))

    coeffs = inner_map(scalar_invts)
    assert_allclose(coeffs, cr._f(convert_arg(scalar_invts), convert_arg(p)))

    outputs = coeff_form_map((coeffs, form_invts))
    assert_allclose(outputs, cr(inputs))


if __name__ == "__main__":
    test_2d_torch()
