import torch
import torch.autograd.functional as ad
from .tensor import Tensor, tensor
from pyadjoint_utils.block import Block
from pyadjoint.enlisting import Enlist
from typing import TypeVar, Callable, Iterable, Sequence, Optional, Union
from functools import wraps, partial
from pyadjoint.tape import get_working_tape, stop_annotating, annotate_tape
from pyadjoint.overloaded_type import (
    OverloadedType,
    register_overloaded_type,
    create_overloaded_object,
)
from pyadjoint_utils.identity import JacobianIdentity


Function = TypeVar("Function", bound=Callable)
OverloadedFunction = TypeVar("OverloadedFunction", bound=Function)


def _compiled_jvp(f, nojit=False):
    def _jvp(inputs, v):
        return ad.jvp(f, inputs, v=v)

    return _jvp if nojit else torch.jit.script(_jvp)


def _compiled_vjp(f, nojit=False):
    def _vjp(inputs, v):
        return ad.vjp(f, inputs, v=v)

    return _vjp if nojit else torch.jit.script(_vjp)


class TorchBlock(Block):
    def __init__(
        self,
        func,
        args,
        outputs,
        jit_mode="script",
        nojit=False,
        argnums=None,
        pointwise=False,
        out_pointwise=None,
        compile_jacobian_stack=True,
    ):
        super().__init__()
        self._func = func
        self._args = list(args)
        jit_modes = {"trace", "script"}
        if jit_mode is not None and jit_mode not in jit_modes:
            raise ValueError(f"JIT mode must be in the set {jit_modes}, not {jit_mode}")
        self._jit_mode = jit_mode
        self._nojit = True  # nojit
        for out in outputs:
            self.add_output(out.create_block_variable())

        self._saved_outs = outputs
        num_inputs, num_outputs = len(args), len(outputs)
        self._single_output = num_outputs == 1
        self._argnums = argnums if argnums is not None else range(num_inputs)
        self._tlm_argnums = self._argnums
        self._tlm_outnums = tuple(range(num_outputs))
        self._diff_func = self._get_reduced_output_diff_func(self._tlm_outnums)
        for idx in self._argnums:
            self.add_dependency(args[idx])
        self._in_pointwise = (
            (pointwise,) * num_inputs if isinstance(pointwise, bool) else pointwise
        )
        self._any_pointwise = any(self._in_pointwise)
        if out_pointwise is None:
            out_pointwise = self._any_pointwise
        self._out_pointwise = (
            (out_pointwise,) * num_outputs
            if isinstance(out_pointwise, bool)
            else out_pointwise
        )
        if len(self._in_pointwise) != num_inputs:
            raise ValueError(
                f"len(pointwise) != num_inputs. For each input, you must specify if it is defined pointwise. ({len(self._in_pointwise)} != {num_inputs})"
            )
        if len(self._out_pointwise) != num_outputs:
            raise ValueError(
                f"len(pointwise) != num_outputs. For each output, you must specify if it is defined pointwise. ({len(self._out_pointwise)} != {num_outputs})"
            )
        if any(self._in_pointwise) and not any(self._out_pointwise):
            raise ValueError(
                "There is a pointwise input and no pointwise outputs. If an input is specified as pointwise, then at least one output must be specified as pointwise."
            )
        if any(self._out_pointwise) and not any(self._in_pointwise):
            raise ValueError(
                "There is a pointwise output and no pointwise inputs. If an output is specified as pointwise, then at least one input must be specified as pointwise."
            )

        self._compiled_jacobian = False
        self._compiled_jacobian_stack = False
        self._vjpfun = (
            list(range(len(self._saved_outs))),
            _compiled_vjp(self._func, self._nojit),
        )
        self._jvpfun = (list(self._argnums), _compiled_jvp(self._func, self._nojit))
        self._vectorize_jacobian = False
        torch._C._debug_only_display_vmap_fallback_warnings(True)

    def _jit(self, f):
        if self._jit_mode and not self._nojit:
            if self._jit_mode == "script":
                return torch.script(f)
            else:
                return torch.trace(f, self._args)
        else:
            return f

    def _replace_params_partial(self, new_params, idxs):
        for i, idx in enumerate(idxs):
            self._args[idx] = new_params[i]

    def _make_partial_diff_func(self, idxs):
        def _pdiff_func(*args):
            param_idxs = [self._argnums[idx] for idx in idxs]
            self._replace_params_partial(args, param_idxs)
            return self._func(*self._args)

        return _pdiff_func

    def _get_reduced_output_diff_func(self, output_ids):
        if self._single_output:
            return self._func

        def diff_func_relevant(*args):
            outputs = self._func(*args)
            outputs = tuple(outputs[idx] for idx in output_ids)
            return outputs

        return diff_func_relevant

    def prepare_recompute_component(self, inputs, relevant_outputs):
        return Enlist(self._func(*inputs))

    def recompute_component(self, inputs, block_variable, idx, prepared):
        if prepared:
            return prepared[idx]
        elif self._saved_outs:
            return self._saved_outs[idx]
        else:
            self._args = list(inputs)
            self._saved_outs = Enlist(self._func(*self._args))
            return self._saved_outs[idx]

    def prepare_evaluate_tlm(self, inputs, tlm_inputs, relevant_outputs):
        out_idx = []
        ipts = []
        tips = []

        for i, (ip, ti) in enumerate(zip(inputs, tlm_inputs)):
            if ti is not None:
                out_idx.append(i)
                ipts.append(ip)
                tips.append(ti)

        anums, jvp = self._jvpfun
        if list(anums) != out_idx:
            self._jvpfun = (
                out_idx,
                _compiled_jvp(self._make_partial_diff_func(out_idx), self._nojit),
            )
            anums, jvp = self._jvpfun

        return jvp(tuple(ipts), tuple(tips))[1]

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        def prod(x):
            p = 1
            for xx in x:
                p *= xx
            return p

        def size(x):
            if hasattr(x, "size"):
                return x.size()
            else:
                return (len(x),)

        return (
            prepared[idx] if prod(size(prepared)) > 1 or idx != 0 else prepared.item()
        )

    def prepare_evaluate_adj(self, inputs, adj_inputs, relevant_dependencies):

        relevant_out_idx = [
            idx for idx, adj in enumerate(adj_inputs) if adj is not None
        ]
        out_idx, vjp = self._vjpfun
        if relevant_out_idx != out_idx:
            f_relevant = self._get_reduced_output_diff_func(out_idx)
            self._vjpfun = (relevant_out_idx, _compiled_vjp(f_relevant, self._nojit))
            out_idx, vjp = self._vjpfun

        return vjp(tuple(inputs), tuple([adj_inputs[i] for i in relevant_out_idx]))[1]

    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        return prepared[idx]

    def prepare_evaluate_tlm_matrix(self, inputs, tlm_inputs, relevant_outputs):
        argnums = []
        for i, x in enumerate(tlm_inputs):
            if x is not None:
                argnums.append(i)
                for j, di_dj in enumerate(x):
                    if (i != j and di_dj is not None) or (
                        i == j and not isinstance(di_dj, JacobianIdentity)
                    ):
                        raise NotImplementedError(
                            "Non-identity tlm_inputs cannot be handled yet!"
                        )
        outnums = [idx for idx, bv in relevant_outputs]
        recompute_dfunc = (tuple(argnums) != tuple(self._tlm_argnums)) or (
            tuple(outnums) != tuple(self._tlm_outnums)
        )
        if recompute_dfunc:
            self._tlm_argnums = argnums
            self._diff_func = self._get_reduced_output_diff_func(outnums)
        if not self._any_pointwise:
            val = ad.jacobian(
                self._diff_func, tuple(inputs), vectorize=self._vectorize_jacobian
            )
            if self._single_output:
                return (val,)

            jac = [None] * len(self.get_outputs())
            for i, idx in enumerate(outnums):
                jac[idx] = val[i]
            return jac

        outputs = [bv.saved_output for bv in self.get_outputs()]
        out_shapes = [
            a.shape[1:] if pw else a.shape
            for a, pw in zip(outputs, self._out_pointwise)
        ]
        n = max(
            [a.shape[0] for a, pw in zip(inputs, self._in_pointwise) if pw]
            + [a.shape[0] for a, pw in zip(outputs, self._out_pointwise) if pw]
        )
        standard_args = []
        for i, (a, pw) in enumerate(zip(inputs, self._in_pointwise)):
            if pw:
                if a.shape[0] not in (1, n):
                    raise ValueError(
                        f"Argument {i} is marked as pointwise but first axis doesn't match expected size ({a.shape[0]} != {n})"
                    )
                s = torch.reshape(a, (a.shape[0], 1, *a.shape[1:]))
                s = torch.broadcast_to(s, (n, *s.shape[1:]))
            else:
                s = torch.broadcast_to(a, (n, *a.shape))
            standard_args.append(s)

        doutput_dinput = [None] * len(self.get_outputs())
        squeeze_didj = partial(torch.squeeze, dim=1)
        for i, out_idx in enumerate(outnums):
            out_pw = self._out_pointwise[out_idx]
            out_rank = len(out_shapes[out_idx])

            def squeeze_didj_pw(x):
                return torch.squeeze(torch.squeeze(x, dim=2 + out_rank), dim=1)

            squeeze_didj_in_pw = partial(torch.squeeze, dim=1 + out_rank)
            # if self._compiled_jacobian_stack and not self._compiled_jacobian:
            di_dinput = [None] * len(argnums)
            self._tlm_matrix_func = lambda *args: ad.jacobian(self._diff_func, args)
            for j, in_idx in enumerate(argnums):
                in_pw = self._in_pointwise[in_idx]
                if out_pw:
                    if self._single_output:
                        di_dj = _pointjac_stack_j(
                            self._tlm_matrix_func,
                            standard_args,
                            j,
                            self._compiled_jacobian_stack,
                        )
                    else:
                        di_dj = _pointjac_stack_ij(
                            self._tlm_matrix_func,
                            standard_args,
                            i,
                            j,
                            self._compiled_jacobian_stack,
                        )
                    if in_pw:
                        #  out_i: (n, *output_shape)
                        #  arg_j: (n, *input_shape)
                        #  di_dj: (n, 1, *output_shape, 1, *input_shape)
                        # di_dj': (n, *output_shape, *input_shape)
                        di_dj = squeeze_didj_pw(di_dj)
                    else:
                        #  out_i: (n, *output_shape)
                        #  arg_j: (*input_shape)
                        #  di_dj: (n, 1, *output_shape, *input_shape)
                        # di_dj': (n, *output_shape, *input_shape)
                        di_dj = squeeze_didj(di_dj)  # _dj)
                elif in_pw:
                    #  out_i: (*output_shape)
                    #  arg_j: (n, *input_shape)
                    #  di_dj: (*output_shape, n, 1, *input_shape)
                    # di_dj': (*output_shape, n, *input_shape)
                    di_dj = _pointjac_stack_ij_out(
                        self._tlm_matrix_func,
                        standard_args,
                        i,
                        j,
                        out_rank,
                        self._compiled_jacobian_stack,
                    )
                    di_dj = squeeze_didj_in_pw(di_dj)
                else:
                    # If neither is defined pointwise, the shapes should be this:
                    #  out_i: (*output_shape)
                    #  arg_j: (*input_shape)
                    #  di_dj: (n, *output_shape, *input_shape)
                    # di_dj': (*output_shape, *input_shape)
                    di_dj = _pointjac_stack_ij(
                        self._tlm_matrix_func,
                        standard_args,
                        i,
                        j,
                        self._compiled_jacobian_stack,
                    )
                    di_dj = di_dj[0, ...]
                di_dinput[j] = di_dj
            doutput_dinput[out_idx] = di_dinput
        return doutput_dinput

    def evaluate_tlm_matrix_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        return prepared[idx]


def _pointjac_stack_j(jacfun, std_args, j, compiled=True):
    if compiled:

        @torch.jit.script
        def compiled_stack_j(std_args):
            torch.stack([jacfun(*args)[j] for args in zip(*std_args)])

        return compiled_stack_j(std_args)
    return torch.stack([jacfun(*args)[j] for args in zip(*std_args)])


def _pointjac_stack_ij(jacfun, std_args, i, j, compiled=True):  # compiled=False):
    if compiled:

        @torch.jit.script
        def compiled_stack_ij(std_args):
            torch.stack([jacfun(*args)[i][j] for args in zip(*std_args)])

        return compiled_stack_ij(std_args)

    return torch.stack([jacfun(*args)[i][j] for args in zip(*std_args)])


def _pointjac_stack_ij_out(jacfun, std_args, i, j, out_ax, compiled=True):
    if compiled:

        @torch.jit.script
        def compiled_stack_ij_out(std_args):
            return torch.stack(
                [jacfun(*args)[i][j] for args in zip(*std_args)], dim=out_ax
            )

        return compiled_stack_ij_out(std_args)
    return torch.stack([jacfun(*args)[i][j] for args in zip(*std_args)], dim=out_ax)


def overload_torch(
    func: Function,
    argnums: Optional[Union[int, Iterable[int]]] = None,
    jit: bool = True,
    pointwise: Union[bool, Sequence[bool]] = False,
    out_pointwise: Optional[Union[bool, Sequence[bool]]] = None,
) -> OverloadedFunction:
    """Creates a pyadjoint-overloaded version of a torch-traceable function.

    :param func: The function to potentially JIT compile and to make differentiable
    :type func: Function
    :param argnums: The numbers of the arguments you want to differentiate
        with respect to. For example, if you have a function f(x,p,w) and
        want the derivative with respect to p and w, pass argnums=(1,2).
    :type argnums: Union[int,Iterable[int]], optional
    :param jit:  If True, JIT compile the function, defaults to True
    :type jit: bool
    :param pointwise: By default, this is false. True means the function performs
        operations on a batch of points. This allows optimizing the Jacobian calculations
        by only computing the diagonal component. If a list, then there should be a bool
        for each argnum.
    :type pointwise: Union[bool,Sequence[bool]], optional
    :param out_pointwise: If any inputs are defined pointwise, this specifies which
        outputs are defined pointwise. If a list, then there should be abool for each
        output. By default, all outputs will be assumed pointwise if any inputs are
        pointwise.
    :type out_pointwise: Union[bool,Sequence[bool]], optional
    """

    if jit:
        func = torch.jit.script(func)

    @wraps(func)
    def _overloaded_func(*args, **kwargs):
        with stop_annotating():
            out = func(*args, **kwargs)
            if torch.jit.is_scripting() or torch.jit.is_tracing():
                return out

        out = Enlist(out)
        overloads = [create_overloaded_object(x) for x in out]
        if annotate_tape():
            block = TorchBlock(
                func,
                args,
                overloads,
                argnums=argnums,
                nojit=(not jit),
                pointwise=pointwise,
                out_pointwise=out_pointwise,
            )
            get_working_tape().add_block(block)
        val = out.delist(overloads)
        if isinstance(val, list):
            return tuple(val)
        return val

    return _overloaded_func
