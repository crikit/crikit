(pyadjoint-api-reference)=

# Pyadjoint Utils API

:::{contents} Table of Contents
:local: true
:backlinks: none

:::

## Core classes

```{eval-rst}
.. todo:: 

   Need to add/update docstrings for all of these.

```

```{eval-rst}
.. automodule:: pyadjoint_utils
```

```{eval-rst}
.. autoclass:: ReducedFunction
   :show-inheritance:

   .. automethod:: __call__
   .. automethod:: jac_action
   .. automethod:: adj_jac_action
   .. automethod:: jac_matrix
```

```{eval-rst}
.. autoclass:: ReducedFunctionNumPy
   :show-inheritance:

   .. automethod:: __call__
   .. automethod:: jac_action
   .. automethod:: adj_jac_action
   .. automethod:: jac_matrix

```

```{eval-rst}
.. autoclass:: Tape
   :show-inheritance:
```

```{eval-rst}
.. autoclass:: Block
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: BlockVariable
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: Control
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: OverloadedType
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: AdjFloat
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: ReducedEquation
   :show-inheritance:
   :members:

```

```{eval-rst}
.. autoclass:: SNESSolver
   :members:

```

## Core functions

```{eval-rst}
.. autofunction:: push_tape
```

```{eval-rst}
.. autofunction:: compute_gradient
```

```{eval-rst}
.. autofunction:: compute_jacobian_matrix
```

```{eval-rst}
.. autofunction:: compute_jacobian_action

```'


## Callbacks

```{eval-rst}
.. autoclass:: Callback
   :show-inheritance:
   .. automethod:: __call__
```

```{eval-rst}
.. autoclass:: FileLoggerCallback
   :show-inheritance:
   .. automethod:: __call__
```

```{eval-rst}
.. autoclass:: CallbackCombiner
   :show-inheritance:
   .. automethod:: __call__
```

## Numpy Backends
```{eval-rst}
.. automodule:: pyadjoint_utils.numpy_backend
```

```{eval-rst}
.. autofunction:: get_default_backend
```

```{eval-rst}
.. autofunction:: get_backend
```

```{eval-rst}
.. autofunction:: set_default_backend
```

```{eval-rst}
.. autoclass:: NumpyBackend
   :members:
```

```{eval-rst}
.. autoclass:: JaxNumpyBackend
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: TorchBackend
   :show-inheritance:
   :members:
```



## Optimization

```{eval-rst}
.. automodule:: pyadjoint_utils.minimize
```

```{eval-rst}
.. autofunction:: minimize
```

## FEniCS adjoint

```{eval-rst}
.. automodule:: pyadjoint_utils.fenics_adjoint
```

```{eval-rst}
.. autofunction:: function_get_local
```

```{eval-rst}
.. autofunction:: function_set_local
```

```{eval-rst}
.. autofunction:: assemble
```

## JAX adjoint

```{eval-rst}
.. automodule:: pyadjoint_utils.jax_adjoint

```

```{eval-rst}
.. autoclass:: ndarray
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autofunction:: array
```

```{eval-rst}
.. autofunction:: asarray
```

```{eval-rst}
.. autofunction:: overload_jax

```

## NumPy adjoint

```{eval-rst}
.. automodule:: pyadjoint_utils.numpy_adjoint
```

```{eval-rst}
.. automodule:: pyadjoint_utils.numpy_adjoint.autograd
```

```{eval-rst}
.. autofunction:: overload_autograd
```

```{eval-rst}
.. autofunction:: overloaded_autograd

```

## TensorFlow-adjoint

```{eval-rst}
.. automodule:: pyadjoint_utils.tensorflow_adjoint
```

```{eval-rst}
.. autofunction:: get_params_feed_dict
```

```{eval-rst}
.. autofunction:: run_tensorflow_graph
```



