import pytest
from crikit.cr.types import PointMap
from crikit.cr.space_builders import DirectSum
from crikit.cr.stdnumeric import RR
from crikit.cr.overloaded import overloaded_point_map
from pyadjoint import ReducedFunctional, taylor_to_dict, stop_annotating
from pyadjoint_utils import (
    AdjFloat,
    Control,
    ReducedFunction,
    taylor_test,
    taylor_to_dict,
    compute_gradient,
    compute_jacobian_action,
    compute_hessian_action,
)
from math import log
from numpy.testing import assert_allclose


@overloaded_point_map
class OverloadedPMTest(PointMap):
    def __init__(self, n=1):
        if n == 1:
            output_space = RR
        else:
            output_space = DirectSum([RR] * n)
        self._n = n
        super().__init__(DirectSum(RR, RR, RR), output_space)

    def __call__(self, point):
        x, p, a = point
        out = x ** p + a
        if self._n == 1:
            return out
        return tuple((i + 1) * out for i in range(self._n))

    def adjoint(self, point, adj_input):
        x, p, a = point
        if self._n != 1:
            adj_input = sum((i + 1) * adj for i, adj in enumerate(adj_input))
        dx = adj_input * x ** (p - 1) * p
        dp = adj_input * x ** p * log(x)
        da = adj_input
        return dx, dp, da

    def tlm(self, point, tlm_input):
        x, p, a = point
        dx, dp, da = tlm_input
        dx *= x ** (p - 1) * p
        dp *= x ** p * log(x)
        out = dx + dp + da
        if self._n == 1:
            return out
        return tuple((i + 1) * out for i in range(self._n))

    def hessian(self, point, adj_input, tlm_input):
        x, p, a = point
        if self._n != 1:
            adj_input = sum((i + 1) * adj for i, adj in enumerate(adj_input))
        dx, dp, da = tlm_input

        hxx = p * (p - 1) * x ** (p - 2)
        hpp = log(x) ** 2 * x ** p
        hpx = x ** (p - 1) * (1 + p * log(x))

        ddx = (hxx * dx + hpx * dp) * adj_input
        ddp = (hpx * dx + hpp * dp) * adj_input
        dda = 0
        return ddx, ddp, dda


def test_overloaded_single_output():
    x = AdjFloat(2)
    p = AdjFloat(4)
    a = AdjFloat(3)
    point = (x, p, a)
    controls = (Control(x), Control(p), Control(a))

    hx = AdjFloat(1.0)
    hp = AdjFloat(2.0)
    ha = AdjFloat(3.0)
    h = (hx, hp, ha)
    v = 1

    pm = OverloadedPMTest()
    output = pm(point)

    with stop_annotating():
        assert_allclose(
            compute_gradient(output, controls, adj_value=v), pm.adjoint(point, v)
        )
        assert_allclose(compute_jacobian_action(output, controls, h), pm.tlm(point, h))

        J = ReducedFunction(output, controls)
        outrecomp = J(point)
        assert_allclose(outrecomp, output)

        assert taylor_test(J, point, h) > 1.9

        keys = ["R0", "R1", "R2"]
        results = taylor_to_dict(J, point, h, v=v)
        for i, k in enumerate(keys):
            assert min(results[k]["Rate"]) > i + 0.9


def test_overloaded_multiple_outputs():
    x = AdjFloat(2)
    p = AdjFloat(4)
    a = AdjFloat(3)
    point = (x, p, a)
    controls = (Control(x), Control(p), Control(a))

    hx = AdjFloat(1.0)
    hp = AdjFloat(2.0)
    ha = AdjFloat(3.0)
    h = (hx, hp, ha)
    v = (2.0, 3.0)
    hessian_value = (0.0, 0.0)

    pm = OverloadedPMTest(n=2)
    outputs = pm(point)
    with stop_annotating():
        assert_allclose(
            compute_gradient(outputs, controls, adj_value=v), pm.adjoint(point, v)
        )
        assert_allclose(compute_jacobian_action(outputs, controls, h), pm.tlm(point, h))
        assert_allclose(
            compute_hessian_action(outputs, controls, h, hessian_value=hessian_value),
            pm.hessian(point, v, h),
        )

        J = ReducedFunction(outputs, controls)
        outrecomp = J(point)
        assert_allclose(outrecomp, outputs)

        assert taylor_test(J, point, h) > 1.9

        keys = ["R0", "R1", "R2"]
        results = taylor_to_dict(J, point, h, v=v)
        for i, k in enumerate(keys):
            assert min(results[k]["Rate"]) > i + 0.9
