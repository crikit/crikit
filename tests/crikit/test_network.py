#!/usr/bin/python3
from math import ceil
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint_utils import *

from crikit.overloaded_network import Network, PlapNetwork
from crikit.network_block import run_network, get_num_network_inputs_outputs

import pytest

# set_up() creates the function space, input function, target function, and network.
def set_up():
    # I don't want a bunch of messages from FEniCS.
    set_log_level(LogLevel.CRITICAL)

    mesh = UnitSquareMesh(5, 5)
    V = FunctionSpace(mesh, "CG", 1)
    return mesh, V


def create_network(output_space, input_spaces, seed=5):
    num_inputs, num_outputs = get_num_network_inputs_outputs(
        output_space, *input_spaces
    )
    output_activation = "linear"
    layers = Network.create_layers(
        num_inputs, num_outputs, output_activation, [5, 6], "sigmoid", seed=seed
    )
    return Network(layers=layers)


@pytest.mark.parametrize("use_twice", [False, True])
def test_network(use_twice):
    mesh, V = set_up()
    u = project(Expression("x[0] + x[1]", degree=1), V)
    net = create_network(V, [V])
    args = (V, u)

    n = run_network(net, *args)
    if use_twice:
        n2 = run_network(net, *args)
        n = n * n2
    J = assemble(n * dx)

    h = project(Expression("x[0]*x[1]", degree=1), V)
    rf = ReducedFunctional(J, Control(u))
    convergence_rate = taylor_test(rf, u, h)
    assert convergence_rate > 1.9

    h = create_network(V, [V], seed=None)
    rf = ReducedFunctional(J, Control(net))
    convergence_rate = taylor_test(rf, net, h)
    assert convergence_rate > 1.9


@pytest.mark.parametrize("use_twice", [False, True])
def test_plap_network(use_twice):
    mesh, V = set_up()
    V_vec = VectorFunctionSpace(mesh, "CG", 1)
    net = PlapNetwork(p=2.5)

    u = project(Expression("x[0]*x[0] + x[1]*sin(2*M_PI*(x[0]+x[1]))", degree=1), V)
    gradu = project(grad(u), V_vec)
    args = (V, u, gradu)

    n = run_network(net, *args)
    if use_twice:
        n2 = run_network(net, *args)
        n = n * n2
    J = assemble(n * dx)

    h = project(Expression("x[0]*x[1]", degree=1), V)
    rf = ReducedFunctional(J, Control(u))
    convergence_rate = taylor_test(rf, u, h)
    assert convergence_rate > 1.9

    h = PlapNetwork(p=2.1)
    rf = ReducedFunctional(J, Control(net))
    convergence_rate = taylor_test(rf, net, h)
    assert convergence_rate > 1.9


def test_learning():
    mesh, V = set_up()
    u = project(Expression("x[0] + x[1]", degree=1), V)
    target = project(Expression("x[0] + x[1] + (x[0]+x[1])*(x[0]+x[1])", degree=1), V)
    net = create_network(V, [V])

    # Calculate the error to get everything set up in Pyadjoint.
    args = (V, u)
    current = run_network(net, *args)
    err = target - current
    J = assemble(err * err * dx)

    # Use minimize() to make the output of the network match the target function.
    Jhat = ReducedFunctional(J, Control(net))
    net_opt = minimize(Jhat, options={"disp": False})

    # Calculate the optimized error.
    J = Jhat(net_opt)

    assert J < 1e-4
