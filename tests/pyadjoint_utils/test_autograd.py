from itertools import product
from pyadjoint import *
from pyadjoint.enlisting import Enlist
from pyadjoint_utils import *
from pyadjoint.overloaded_type import create_overloaded_object as coo
from pyadjoint_utils.numpy_adjoint import *
from pyadjoint_utils.numpy_adjoint.autograd import overloaded_autograd
from functools import wraps
import autograd.numpy as np

import pytest
from numpy.testing import assert_array_equal


@overloaded_autograd(False)
def one_output(A, B):
    C = A * B
    return np.exp(C)


@overloaded_autograd(False)
def two_outputs(A, B):
    C = A * B
    return C, C ** 2


@overloaded_autograd((True, False))
def pointwise_output(A, B):
    C = (A * A) @ B
    return C


@overloaded_autograd(False)
def one_output_kwargs(A, B, got_kwargs=None):
    if got_kwargs is None:
        raise ValueError("Missing keyword argument")
    C = A * B
    return np.exp(C)


@overloaded_autograd((True, False))
def pointwise_output_kwargs(A, B, got_kwargs=None):
    if got_kwargs is None:
        raise ValueError("Missing keyword argument")
    C = (A * A) @ B
    return C


@pytest.mark.parametrize(
    ("f", "use_kwargs"),
    [
        (one_output, False),
        (two_outputs, False),
        (pointwise_output, False),
        (one_output_kwargs, True),
        (pointwise_output_kwargs, True),
    ],
)
def test_overloaded_autograd(f, use_kwargs):
    kwargs = {"got_kwargs": True} if use_kwargs else {}
    if f == two_outputs:
        pytest.raises(ValueError, run_block_tests, f, **kwargs)
    else:
        run_block_tests(f, **kwargs)


def run_block_tests(f, **kwargs):
    np.random.seed(433289)
    n = 5
    m = 3
    A = coo(np.arange(n * m, dtype=float).reshape(n, m) - 5)
    B = coo(np.arange(m, dtype=float) + 5)

    inputs = (A, B)

    controls = tuple(Control(a) for a in inputs)
    outputs = Enlist(f(*inputs, **kwargs))

    rf = ReducedFunction(outputs, controls)

    # Test recompute.
    with stop_annotating():
        A = coo(np.arange(A.size, dtype=float).reshape(*A.shape) - 3)
        B = coo(np.arange(B.size, dtype=float) + 1)

        rf_computed = rf((A, B))
        recomputed = Enlist(f(A, B, **kwargs))
        assert_array_equal(rf_computed, recomputed)

    new_inputs = tuple(np.random.randn(*a.shape) for a in inputs)

    # Test adjoint.
    h = tuple(1e-2 * np.random.randn(*a.shape) for a in inputs)
    v = tuple(np.random.randn(*a.shape) for a in outputs)
    assert taylor_test(rf, new_inputs, h=h, v=v) > 1.9

    # Test tlm.
    h = tuple(1e-2 * np.random.randn(*a.shape) for a in inputs)
    assert taylor_test(rf, new_inputs, h=h) > 1.9

    # Test hessian.
    h = tuple(1e-1 * np.random.randn(*a.shape) for a in inputs)
    v = tuple(np.random.randn(*a.shape) for a in outputs)
    assert taylor_test(rf, new_inputs, h=h, v=v, Hm=None) > 2.9

    # Use taylor_to_dict().
    rate_keys = ["R0", "R1", "R2"]
    results = taylor_to_dict(rf, new_inputs, h, v=v)
    for order, k in enumerate(rate_keys):
        assert min(results[k]["Rate"]) > order + 0.9

    # Test tlm matrix.
    tlm_matrix = rf.jac_matrix()

    dJdm = [0] * len(outputs)
    v = [coo(v_i) for v_i in v]
    for i in range(len(outputs)):
        for j in range(len(controls)):
            if f.pointwise[j]:
                rank = len(h[j].shape) - 1
                hdot_shape = tlm_matrix[i][j].shape[
                    : len(tlm_matrix[i][j].shape) - rank
                ]
                w = (
                    np.tensordot(a, b, axes=rank)
                    for a, b in zip(tlm_matrix[i][j], h[j])
                )
                hdot = np.zeros(hdot_shape)
                for k, hdot_k in enumerate(w):
                    hdot[k, ...] = hdot_k
            else:
                hdot = np.tensordot(tlm_matrix[i][j], h[j], axes=len(h[j].shape))
            dJdm[i] += v[i]._ad_dot(hdot)
    dJdm = [coo(dJdm_i) for dJdm_i in dJdm]

    rate_keys = ["R0", "R1"]
    results = taylor_to_dict(rf, new_inputs, h, dJdm=dJdm[0], Hm=0, v=v)
    for order, k in enumerate(rate_keys):
        assert min(results[k]["Rate"]) > order + 0.9
