from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint_utils import *

from pyadjoint.overloaded_type import create_overloaded_object
from pyadjoint_utils.fenics_adjoint import function_get_local, function_set_local
import numpy as np
from numpy.testing import assert_allclose, assert_equal


def test_annotations():

    mesh = UnitSquareMesh(3, 3)

    degree = 1
    scheme = "default"
    quad_params = {
        "quadrature_degree": 1,
        "quadrature_rule": "default",
        "representation": "quadrature",
    }
    el = TensorElement(
        "Quadrature", mesh.ufl_cell(), shape=(), degree=degree, quad_scheme=scheme
    )
    V = FunctionSpace(mesh, el)

    el = TensorElement(
        "Quadrature", mesh.ufl_cell(), shape=(2,), degree=degree, quad_scheme=scheme
    )
    V_vec = FunctionSpace(mesh, el)

    for S in [V, V_vec]:
        u = Function(S)
        u_rev = Function(S)
        h_u = Function(S)

        np_data = np.arange(len(u.vector())).reshape((-1, *u.ufl_shape))
        data = create_overloaded_object(np_data)

        h_data = create_overloaded_object(np.random.rand(*np_data.shape))
        h_u.vector()[:] = h_data.flatten()

        # Test function_set_local.
        with push_tape():
            function_set_local(u, data)
            assert_equal(u.vector()[:], data.flatten())
            rf = ReducedFunction(u, Control(data))

            J = assemble(
                inner(u, u) * inner(u, u) * inner(u, u) * dx(metadata=quad_params)
            )
            Jhat = ReducedFunctional(J, Control(data))

            # Test recompute.
            assert Jhat(data) == J

            # Test adjoint.
            assert taylor_test(Jhat, data, h_data) > 1.9

            # Test Jacobian matrix.
            dd_dd = np.zeros(data.shape * 2)
            indices = np.indices(data.shape)
            diagonal = tuple([i for i in indices]) * 2
            dd_dd[diagonal] = 1
            jac = rf.jac_matrix()
            assert isinstance(jac, (np.ndarray, JacobianIdentity))
            if isinstance(jac, np.ndarray):
                assert_equal(jac, dd_dd)

            # Test Jacobian action.
            assert_equal(rf.jac_action(h_data).vector()[:], h_u.vector()[:])

        # Test function_get_local.
        u.vector()[:] = data.flatten()
        with push_tape():
            extracted = function_get_local(u)
            assert_equal(u.vector()[:], extracted.flatten())
            rf = ReducedFunction(extracted, Control(u))

            index = [
                0,
            ] * (1 + len(u.ufl_shape))
            index[0] = 2
            index = tuple(index)
            J = extracted[index] * extracted[index]
            Jhat = ReducedFunctional(J, Control(u))

            # Test recompute.
            assert Jhat(u) == J

            # Test adjoint.
            assert taylor_test(Jhat, u, h_u) > 1.9

            # Test Jacobian matrix.
            jac = rf.jac_matrix()
            assert isinstance(jac, (np.ndarray, JacobianIdentity))
            if isinstance(jac, np.ndarray):
                assert_equal(jac, dd_dd)

            # Test Jacobian action.
            assert_equal(rf.jac_action(h_u), h_data)
