from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.covering import get_composite_cr, set_default_covering_params
from crikit.cr.ufl import UFLExprSpace, UFLFunctionSpace, CR_UFL_Expr
from crikit.cr import TorchTensor, TorchFunction
from crikit.cr.space_builders import DirectSum
from pyadjoint_utils import overload_torch, tensor, AdjFloat

import torch

try:
    from functorch import vmap
except Exception:

    class _ArgHandler:
        ax = None
        pos = 0
        arg = None

        def __init__(self, *args):
            self.ax, self.pos, self.arg = args

        def __iter__(self):
            self.pos = 0
            return self

        def __next__(self):
            if self.ax is None:
                return self.arg
            elif self.ax == 0:
                if self.pos < self.arg.shape[0]:
                    self.pos += 1
                    return self.arg[self.pos - 1]
                else:
                    raise StopIteration
            else:
                raise NotImplementedError

    # this vmap only works for one single output which is vmapped (i.e. out_dims=(0,)), and only for input_dims None or 0
    def vmap(f, in_dims=(0,)):
        def f_vmapped(*args):
            handlers = [_ArgHandler(ax, 0, arg) for ax, arg in zip(in_dims, args)]
            return torch.stack([f(*point) for point in zip(*handlers)])

        return f_vmapped


from functools import partial


def test_torch_covering_one_param():
    mesh = UnitSquareMesh(3, 3)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 1)
    U = TensorFunctionSpace(mesh, "P", 1)
    out_space = UFLFunctionSpace(U)
    u_0 = project(Expression(("x[0]", "x[1]"), degree=1), V)
    g = sym(grad(u_0))
    ufl_source = UFLExprSpace(g)

    p = tensor([1.5, -4.0])
    ufl_p = (AdjFloat(1.5), AdjFloat(-4.0))

    expr = (ufl_p[0] ** 2) * dot(g, g) * (ufl_p[1] ** 2)

    @partial(vmap, in_dims=(0, None))
    def torch_func(g, p):
        return (p[0] ** 2) * g @ g * (p[1] ** 2)

    torch_inner_cr = TorchFunction(
        torch_func, [(-1, 2, 2), (2,)], (-1, 2, 2), jit=False
    )
    ufl_inner_cr = CR_UFL_Expr(ufl_source, expr, {UFLExprSpace(g).point(): 0})

    quad_params = {"quadrature_degree": 2}
    set_default_covering_params(domain=domain, quad_params=quad_params)
    torch_cr = get_composite_cr(
        DirectSum([ufl_source, TorchTensor((2,))]), torch_inner_cr, out_space
    )
    ufl_cr = get_composite_cr(ufl_source, ufl_inner_cr, out_space)

    sigma = torch_cr((g, p))
    assert errornorm(sigma, ufl_cr(g)) <= 1.0e-7

    err = assemble(inner(sigma, sigma) * dx)
    rf = ReducedFunction(err, Control(p))
    h = tensor(torch.rand(2))
    v = AdjFloat(1)
    # test adjoint
    assert taylor_test(rf, p, h, v=v) >= 1.9
