from functools import wraps
import time


class _BasicVmapArgHandler:
    ax = None
    pos = 0
    arg = None

    def __init__(self, *args):
        self.ax, self.pos, self.arg = args

    def __iter__(self):
        self.pos = 0
        return self

    def __next__(self):
        if self.ax is None:
            return self.arg
        elif self.ax == 0:
            if self.pos < self.arg.shape[0]:
                self.pos += 1
                return self.arg[self.pos - 1]
            else:
                raise StopIteration
        else:
            raise NotImplementedError


class NumpyBackend:
    """A {class}`NumpyBackend` implements the numpy API
    plus `jit`, `vmap`, and `rand`.

    """

    def __init__(self, numpy=None, name="numpy"):
        if numpy is None:
            import autograd.numpy as np

            numpy = np

        self.name = name
        self.np = numpy

    def __getattr__(self, attr, default=None):
        try:
            return getattr(self.np, attr, default)
        except AttributeError:
            return default

    def jit(self, f, *args, **kwargs):
        """The base jit() simply acts as a pass-through and returns the function
        as-is
        """
        return f

    def vmap(self, f, in_axes=(0,)):
        """The base vmap() implements something like `jax.vmap` but in pure
        Python, and that can only handle one output that is vmapped across axis 0.
        Additionally, each element of `in_axes` must be either `0` or `None`.
        """

        @wraps(f)
        def f_vmapped(*args, **kwargs):
            handlers = [
                _BasicVmapArgHandler(ax, 0, arg) for ax, arg in zip(in_axes, args)
            ]
            return self.np.stack([f(*point) for point in zip(*handlers)])

        return f_vmapped

    def init_rng(self, seed=None):
        """Initialize the random number generator by setting the seed. If `None`, use
        `int(time.time())`
        """
        if seed is None:
            seed = int(time.time())

        self.np.random.seed(seed=seed)

    def randn(self, *args):
        """Returns a sample from the standard Normal distribution with shape specified by
        `args`
        """
        return self.np.random.randn(*args)

    def vjp(self, f, *args):
        raise NotImplementedError

    def script_if_tracing(self, f):
        return f

    def index_update(self, x, idx, y):
        x[idx] = y
        return x
