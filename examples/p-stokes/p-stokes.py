import tensorflow

from crikit.cr import Callable, CR, JAXArrays, P_Laplacian, cr_function_shape
from crikit.cr.fe import assemble_with_cr
from crikit.cr.ufl import create_ufl_standins, UFLExprSpace, UFLFunctionSpace
from crikit.cr.quadrature import make_quadrature_spaces
from crikit.covering import get_composite_cr, set_default_covering_params
from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.invariants import TensorType
from crikit.loss import integral_loss
from crikit.observer import (
    u_observer,
    SurfaceObserver,
    SubdomainObserver,
    AdditiveRandomFunction,
)
from jax import numpy as jnp
from plot_flow import plot_flow
from pyadjoint import *
from pyadjoint.enlisting import Enlist
from pyadjoint.overloaded_type import create_overloaded_object as coo
from pyadjoint.reduced_functional_numpy import ReducedFunctionalNumPy
from pyadjoint_utils.fenics_adjoint import homogenize_bcs
from pyadjoint_utils.jax_adjoint import overload_jax
from pyadjoint_utils.tape_block import record_tape_block
from pyadjoint_utils import *
import pyadjoint
from ufl import zero

from crikit.projection import project

import argparse
import crikit.utils as utils
import numpy as onp
import jax
import torch

from cr_plotter import InvariantCRPlotter
from network_builders import FICNN, MLP, PlapNN, MonotonicNN, TorchFeedForwardNet
from experiment import Experiment

try:
    import matplotlib.pyplot as plt
except:
    pass

import logging

logging.getLogger("FFC").setLevel(logging.WARNING)


def optimize_params(Jhat, display_params=False, torch=False, network=None, **kwargs):
    record_file = kwargs.pop("record_file", "data.csv")

    # Don't display minimize() info by default.
    options = kwargs.pop("options", {})
    options["disp"] = kwargs.pop("disp", False)

    losses = []
    mag_grads = []
    if record_file:
        # Open up the data file and write the column headers.
        with open(record_file, "w") as f:
            f.write("loss,grad_loss\n")

    # Define a function that appends to this data file.
    def minimize_callback(xk, state=None):
        # Record the loss and loss gradient.
        loss = Jhat.functional.block_variable.checkpoint
        grad_loss = [c.get_derivative() for c in Jhat.controls]
        grad_loss = sum(onp.sqrt(coo(g)._ad_dot(g)) for g in grad_loss)

        losses.append(loss)
        mag_grads.append(grad_loss)
        if record_file:
            with open(record_file, "a") as f:
                f.write("%g,%g\n" % (loss, grad_loss))

    if display_params:
        orig_cb = Jhat.eval_cb_pre

        def eval_cb_pre(p):
            print("p =", p)
            orig_cb(p)

        Jhat.eval_cb_pre = eval_cb_pre

    kwargs["method"] = kwargs.get("method", "L-BFGS-B")
    # kwargs['bounds'] = kwargs.get('bounds', (1.5, 4.0))
    if kwargs["method"] == "L-BFGS-B":
        # Define good defaults for some optimizer parameters.
        options["eps"] = kwargs.pop("eps", 1)
        options["gtol"] = kwargs.pop("gtol", args.gtol)
        options["ftol"] = kwargs.pop("ftol", args.ftol)

    params = minimize(Jhat, options=options, callback=minimize_callback, **kwargs)

    # Pyadjoint bug: minimize doesn't respect the enlistment of Jhat's controls.
    params = Jhat.controls.delist(Enlist(params))
    iter_info = [losses, mag_grads]
    return params, iter_info


def run_ufl_cr(args, ex, obs, loss, observer=None):
    ############# Run an experiment to get predicted observations. ############
    #############   Note: Different CR parameters are used.        ####
    #############         The goal will be to figure out the real  ####
    #############         p starting out at a wrong p.             ####

    p = Constant(args.startp, name="p")

    def cr(epsilon):
        e2 = tr(dot(epsilon, epsilon))
        mu = (e2 + eps2) ** ((p - 2) / 2)
        return mu * epsilon

    with record_tape_block(name="UFL_experiment"):
        pred_u = ex.run(cr, u_observer, ufl=True, disp=args.debug)
        pred = observer(pred_u) if observer is not None else pred_u

    plt.figure()
    plot_solution(pred_u, filename="ufl-pred-" + args.file)
    plt.title("Solution with p = %g (Untrained)" % p)
    utils.saveplot()

    ############# Calculate loss and gradient of loss wrt p and u. ############
    err = loss(obs, pred)
    print("   Initial p: %g" % p)
    print("Initial loss: %g" % err)
    if onp.isnan(err):
        raise ValueError("Error: loss is NaN")

    Jhat = ReducedFunctional(err, Control(p))
    if args.verify:
        h = Constant(0.0625, name="h")
        epsilons = [1.0 / 2 ** i for i in range(12)]
        taylor_test(Jhat, p, h, epsilons=epsilons)

    ############# Optimize ##################
    if args.opt:
        print()
        print("Optimizing...")
        new_params, iter_info = optimize_params(
            Jhat,
            record_file="ufl_cr.csv",
            display_params=args.opt_params,
            disp=args.opt_output,
        )

        err = Jhat(new_params)
        print("   Final p: %g" % new_params)
        print("Final loss: %g" % err)

        new_pred_u = Control(pred_u).tape_value()

        plt.figure()
        plot_solution(new_pred_u, filename="ufl-pred-trained-" + args.file)
        plt.title("Solution with p_opt = %g (Trained)" % new_params)
        utils.saveplot()

    if args.show:
        plt.show()


def make_jax_cr(p, dim, energy=False):
    input_types = (TensorType.make_symmetric(2, dim),)
    if energy:
        output_type = TensorType.make_scalar()

        def cr_func(scalar_invts, p):
            energy = (1 / p) * (scalar_invts[1] + eps2) ** (p / 2)
            return energy

    else:
        output_type = TensorType.make_symmetric(2, dim)

        def cr_func(scalar_invts, p):
            isotropic_scale = (scalar_invts[1] + eps2) ** ((p - 2) / 2)
            return jnp.array([0, isotropic_scale])

    cr = CR(
        output_type,
        input_types,
        cr_func,
        params=(p,),
        strain_energy=energy,
    )
    return cr


def run_jax_cr(args, ex, obs, loss, observer=None, energy=False, plap=False):
    p_np = array(jnp.array(args.startp))
    if plap:
        if energy:
            raise ValueError(
                "P_Laplacian class is not set up to output the strain energy yet."
            )
        cr = P_Laplacian(p_np, args.dim, eps2)
    else:
        cr = make_jax_cr(p_np, dim=mesh.geometric_dimension(), energy=energy)

    with record_tape_block(name="JAX-Experiment"):
        pred_u = ex.run(cr, observer=None, ufl=False, disp=args.debug)
        pred = observer(pred_u) if observer is not None else pred_u

    plt.figure()
    plot_solution(pred_u, filename="jax-pred-" + args.file)
    plt.title(f"Solution with p = {p_np}")
    utils.saveplot()

    err = loss(obs, pred)
    print(f"Initial loss is {err}")

    with stop_annotating():
        Jhat = ReducedFunctional(err, Control(p_np))
        if args.verify:
            # get_working_tape().visualise('logs', True, True)
            h = 0.1 * array(jnp.array(onp.random.randn(*p_np.shape)))
            epsilons = [1.0 / 2 ** i for i in range(2, 10)]
            pyadjoint.taylor_test(Jhat, p_np, h)  # , epsilons=epsilons)

        if args.opt:
            print(f"Optimizing CR {cr}")
            bounds = (1.5, 4.0)
            new_params, iter_info = optimize_params(
                Jhat,
                record_file="jax_cr_p.csv",
                display_params=args.opt_params,
                disp=args.opt_output,
                bounds=bounds,
                method="SLSQP",
            )
            err = Jhat(new_params)
            print(f"Final params: {new_params}")
            print(f"Final loss: {err}")
            cr.p = new_params
            new_u = Control(pred_u).tape_value()

            plt.figure()
            plot_solution(new_u, filename="jax-pred-trained-" + args.file)
            plt.title(f"Solution with params p = {new_params}")
            utils.saveplot()

        if args.show:
            plt.show()


def make_torch_network_cr(dim, layer_sizes=[5, 5, 5, 5, 5, 2]):
    input_types = (TensorType.make_symmetric(2, dim),)
    output_type = TensorType.make_symmetric(2, dim)

    try:
        import functorch
    except ImportError:
        print(
            "Error, functorch is required for mode `torch_network`! You can install it with the install-functorch.sh script that is provided in the top-level of the CRIKit repository"
        )
        raise

    n_scalar_invts, n_form_invts = cr_function_shape(output_type, input_types)
    network, params = functorch.make_functional(
        TorchFeedForwardNet(n_scalar_invts, layer_sizes)
    )
    params = list(map(tensor, params))

    def net_apply(scalar_invts, *params):
        return network(params, scalar_invts)

    cr = CR(
        output_type, input_types, net_apply, params=params, jit=False, backend="torch"
    )
    return cr, network, params


def run_torch_network_cr(args, ex, obs, loss, observer=None):
    cr, net, params = make_torch_network_cr(args.dim)

    with record_tape_block(name="Torch-Experiment"):
        pred_u = ex.run(cr, observer=None, ufl=False, disp=args.debug)
        pred = observer(pred_u) if observer is not None else pred_u

    plt.figure()
    plot_solution(pred_u, filename="torch-pred-" + args.file)
    plt.title(f"Solution with initial params")
    utils.saveplot()

    err = loss(obs, pred)
    print(f"Initial loss is {err}")

    with stop_annotating():
        Jhat = ReducedFunctional(err, [Control(p) for p in params])
        if args.verify:
            # get_working_tape().visualise('logs', True, True)
            h = [0.1 * tensor(torch.rand(*x.shape)) for x in params]
            epsilons = [1.0 / 2 ** i for i in range(2, 10)]
            pyadjoint.taylor_test(Jhat, params, h)  # , epsilons=epsilons)

        if args.opt:
            print(f"Optimizing CR {cr}")
            new_params, iter_info = optimize_params(
                Jhat,
                record_file="torch_network_theta.csv",
                display_params=args.opt_params,
                disp=args.opt_output,
                method="L-BFGS-B",
            )
            err = Jhat(new_params)
            print(f"Final params: {new_params}")
            print(f"Final loss: {err}")
            new_u = Control(pred_u).tape_value()

            plt.figure()
            plot_solution(new_u, filename="torch-pred-trained-" + args.file)
            plt.title(f"Solution with params theta = {new_params}")
            utils.saveplot()

        if args.show:
            plt.show()


def make_jax_network_cr(dim, energy=False, plap=False):
    input_types = (TensorType.make_symmetric(2, dim),)
    if energy:
        output_type = TensorType.make_scalar()
    else:
        output_type = TensorType.make_symmetric(2, dim)

    cr = CR(output_type, input_types, strain_energy=energy)
    num_inputs = cr.cr_input_shape[0]
    num_outputs = cr.num_scalar_functions

    if plap:
        network = PlapNN(args.startp, eps2=eps2, energy=energy)
    elif energy:
        network = FICNN(num_outputs, layer_sizes=[5, 5])
    else:
        network = MonotonicNN(num_outputs, layer_sizes=[5, 5])

    # Initialize weights by giving dummy input.
    key1, key2 = jax.random.split(jax.random.PRNGKey(0))
    x = jax.random.normal(key1, (num_inputs,))
    params = network.init(key2, x)

    # Print out network information
    network_shapes = jax.tree_map(lambda p: p.shape, params)
    print("Network info:", network_shapes)

    # Make CR function that acts on the flattened param tree.
    flat_params, tree_info = jax.tree_util.tree_flatten(params)
    overloaded_flat_params = [coo(p) for p in flat_params]

    def network_eval(inputs, *flat_params):
        params = jax.tree_util.tree_unflatten(tree_info, flat_params)
        return network.apply(params, inputs)

    # Set bounds (just to enforce nonnegativity on certain params).
    nonnegatives = []
    for key in params["params"]:
        if key.startswith("nonnegative_"):
            nonnegatives.append(params["params"][key]["kernel"])
    lower_bounds = [
        0 if any(p is nn for nn in nonnegatives) else -onp.inf for p in flat_params
    ]
    upper_bounds = [onp.inf] * len(flat_params)
    bounds = [lower_bounds, upper_bounds]

    # Create CR.
    cr = CR(
        output_type,
        input_types,
        network_eval,
        jit=True,
        params=overloaded_flat_params,
        strain_energy=energy,
    )
    return cr, overloaded_flat_params, bounds, network_eval


def run_jax_network_cr(args, ex, obs, loss, observer=None, **cr_kwargs):
    cr, params, bounds, network_eval = make_jax_network_cr(
        dim=mesh.geometric_dimension(), **cr_kwargs
    )

    with record_tape_block(name="JAX-Network-Experiment"):
        pred_u = ex.run(cr, observer=None, ufl=False, disp=args.debug)
        pred = observer(pred_u) if observer is not None else pred_u

    plt.figure()
    plot_solution(pred_u, filename="jax-network-pred-" + args.file)
    plt.title(f"Solution with initial network")
    utils.saveplot()

    err = loss(obs, pred)
    print(f"Initial loss is {err}")

    with stop_annotating():
        # Plot correct CR output.
        cr_plotter = InvariantCRPlotter(
            ex.mesh,
            p=args.ptrue,
            eps2=eps2,
            energy=cr._strain_energy,
            INVARIANT_CMAP=INVARIANT_CMAP,
            XY_CMAP=XY_CMAP,
        )
        true_cb, true_cb_xy = cr_plotter.plot_correct_all(split(obs_u)[0])

        # Plot untrained CR output.
        network_eval_overloaded = overload_jax(
            jax.vmap(network_eval, in_axes=cr._vmap_axes), jit=True
        )
        net_eval_func = lambda inputs: network_eval_overloaded(inputs, *params)
        if cr._strain_energy:
            net_eval_cr = Callable(
                JAXArrays((-1,) + cr.cr_input_shape), JAXArrays((-1,)), net_eval_func
            )
        else:
            net_eval_cr = Callable(
                JAXArrays((-1,) + cr.cr_input_shape),
                JAXArrays((-1, cr.num_scalar_functions)),
                net_eval_func,
            )

        strain = sym(grad(split(obs_u)[0]))
        cr_plotter.plot_cr_all(
            cr,
            params,
            strain,
            true_cb,
            true_cb_xy,
            net_eval_cr=net_eval_cr,
            prefix="Untrained CR: ",
        )

        # Verify derivative and then optimize.
        controls = [Control(p) for p in params]
        Jhat = ReducedFunctional(err, controls)
        if args.verify:
            h = [
                1e-4 * array(jnp.array(onp.random.randn(*p.shape) ** 2)) for p in params
            ]
            epsilons = [1.0 / 2 ** i for i in range(12)]
            d = taylor_to_dict(Jhat, params, h, Hm=0, epsilons=epsilons)
            from pprint import pprint

            pprint(d)

        if args.opt:
            print("Optimizing CR")
            new_params, iter_info = optimize_params(
                Jhat,
                record_file="jax_cr_network.csv",
                bounds=bounds,
                display_params=args.opt_params,
                disp=args.opt_output,
            )
            err = Jhat(new_params)
            print(f"Final params: {new_params}")
            print(f"Final loss: {err}")
            new_u = Control(pred_u).tape_value()

            plt.figure()
            plot_solution(new_u, filename="jax-network-pred-trained-" + args.file)
            plt.title(f"Solution with trained network")
            utils.saveplot()

            # Plot trained CR output.
            cr.set_params(new_params)
            net_eval_func = lambda inputs: network_eval_overloaded(inputs, *new_params)
            if cr._strain_energy:
                net_eval_cr = Callable(
                    JAXArrays((-1,) + cr.cr_input_shape),
                    JAXArrays((-1,)),
                    net_eval_func,
                )
            else:
                net_eval_cr = Callable(
                    JAXArrays((-1,) + cr.cr_input_shape),
                    JAXArrays((-1, cr.num_scalar_functions)),
                    net_eval_func,
                )

            cr_plotter.plot_cr_all(
                cr,
                new_params,
                strain,
                true_cb,
                true_cb_xy,
                net_eval_cr=net_eval_cr,
                prefix="Trained CR: ",
            )

            # Plot loss.
            plt.figure()
            plt.semilogy(iter_info[0])
            plt.xlabel("Iteration #")
            plt.ylabel("Loss")
            plt.title("Loss during optimization")
            utils.saveplot()

            plt.figure()
            plt.semilogy(iter_info[1])
            plt.xlabel("Iteration #")
            plt.ylabel("|grad loss|")
            plt.title("Grad loss during optimization")
            utils.saveplot()

    if args.show:
        plt.show()


def plot_solution(w, filename):
    if args.dim == 3:
        plot_flow(obs_u, filename=filename, cmap=EX_CMAP)
    else:
        u, p = w.split()
        f = plot(u, cmap=EX_CMAP)
        plt.colorbar(f)


if __name__ == "__main__":

    modes_info = [
        ("ufl", "The stress CR is implemented using UFL operators."),
        (
            "jax_plap",
            "The stress CR is implemented with JAX as a class `P_Laplacian` that inherits from the `CR` class.",
        ),
        ("jax", "The stress CR is implemented with a JAX function and the `CR` class."),
        (
            "jax_energy",
            "The energy CR is implemented with a JAX function and the `CR` class.",
        ),
        (
            "network",
            "The stress CR is implemented with a Flax input-convex network and the `CR` class.",
        ),
        (
            "network_energy",
            "The energy CR is implemented with a Flax monotonic network and the `CR` class.",
        ),
        (
            "plap_network",
            "This is the same as `jax` but the Flax interface is used to define the p-Stokes JAX function.",
        ),
        (
            "plap_network_energy",
            "This is the same as `jax_energy` but the Flax interface is used to define the p-Stokes JAX function.",
        ),
        (
            "torch_network",
            "Uses a PyTorch neural network with the `CR` class",
        ),
    ]
    modes_help = "There are several different CR implementations that can be tested.\n"
    modes_help += "\n".join(f"- `{mode}`: {desc}" for mode, desc in modes_info)
    modes = [mode for mode, info in modes_info]

    parser = argparse.ArgumentParser(
        description=modes_help, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    utils.add_bool_arg(parser, "debug", False, "Print extra output when solving PDE.")
    utils.add_bool_arg(
        parser, "viz_tape", False, "Call Tape.visualise() after everything is run."
    )
    utils.add_bool_arg(
        parser,
        "show",
        True,
        "Don't show plots interactively. This is useful when combined with `--save_dir`.",
    )

    utils.add_bool_arg(
        parser,
        "robin",
        True,
        "Use Dirichlet boundary conditions (BCs) instead of Robin BCs. The Robin BCs are essentially relaxations of the Dirichlet ones.",
    )
    utils.add_bool_arg(
        parser,
        "verify",
        True,
        "Skip Taylor test verification, which can save some time if you already know the gradient is correct.",
    )
    utils.add_bool_arg(
        parser, "opt", False, "Run the optimization for the CR parameters."
    )
    utils.add_bool_arg(
        parser, "opt_output", False, "Print optimization output from scipy.minimize"
    )
    utils.add_bool_arg(
        parser,
        "opt_params",
        False,
        "Print CR parameters at each optimization iteration.",
    )

    parser.add_argument("--seed", type=int, help="Seed for RNG.", default=0)
    parser.add_argument(
        "-p",
        "--ptrue",
        type=float,
        help="Value to use for ground-truth `p`.",
        default=3,
    )
    parser.add_argument(
        "--startp",
        type=float,
        help="Value to use for initial `p` when using a `p`-Stokes candidate CR.",
        default=2.5,
    )
    parser.add_argument(
        "-f",
        "--file",
        type=str,
        help="File to write velocity solution when using 3D mesh. This is useful because it takes so long to run.",
        default="flow_data.npz",
    )
    parser.add_argument(
        "-o",
        "--observer",
        type=str,
        choices=["full", "outflow"],
        help="Observer function to use for loss. There are two options. - `full`: The full PDE solution is observed. - `outflow`: The solution is observed on the outflow boundary.",
        default="full",
    )
    parser.add_argument(
        "-d",
        "--dim",
        type=int,
        choices=[2, 3],
        help="Number of spatial dimensions.",
        default=2,
    )
    parser.add_argument(
        "-g",
        "--grid",
        type=int,
        help="Grid size per dimension. In 2D, the grid is `g x g` and in 3D, `g x g x g`.",
        default=5,
    )
    parser.add_argument(
        "--epsilon",
        type=float,
        help="Regularization value used to avoid singularities in power law.",
        default=1e-5,
    )
    parser.add_argument(
        "--quad",
        type=int,
        help="Quadrature degree used to assemble PDE weak form.",
        default=6,
    )

    parser.add_argument(
        "--gtol",
        type=float,
        help="Optimization stop condition for gradient.",
        default=1e-13,
    )
    parser.add_argument(
        "--ftol",
        type=float,
        help="Optimization stop condition for decrease in residual.",
        default=1e-13,
    )
    parser.add_argument(
        "--save_dir",
        type=str,
        help="Directory to save figures to. By default, the figures won't be saved.",
        default="",
    )
    parser.add_argument(
        "--format",
        type=str,
        help="Image format to use for saving figures.",
        default="png",
    )

    parser.add_argument(
        "--ex_cmap",
        type=str,
        help="Matplotlib color map for plotting the PDE solution in physical space (as a function of the mesh coordinates).",
        default="magma",
    )
    parser.add_argument(
        "--invariant_cmap",
        type=str,
        help="Matplotlib color map for plotting the CR as a function of the invariants.",
        default="jet",
    )
    parser.add_argument(
        "--xy_cmap",
        type=str,
        help="Matplotlib color map for plotting the CR in physical space.",
        default="spring",
    )

    parser.add_argument(
        "mode", choices=modes, help="Which implementation to use for the CR. "
    )
    parser.add_argument(
        "-std",
        "--noise_std",
        type=float,
        help="Standard deviation of noise applied to observations.",
        default=0.0,
    )

    args = parser.parse_args()

    if not args.debug:
        set_log_level(LogLevel.CRITICAL)

    if args.save_dir != "":
        utils.FIGURE_SAVE_DIR = args.save_dir
        try:
            import matplotlib.pyplot as plt

            plt.rc("savefig", format=args.format)
        except Exception as e:
            print("Warning: couldn't set output image format:", e)

    try:
        from matplotlib import cm

        EX_CMAP = cm.get_cmap(args.ex_cmap)
        INVARIANT_CMAP = cm.get_cmap(args.invariant_cmap)
        XY_CMAP = cm.get_cmap(args.xy_cmap)
    except Exception as e:
        print("Warning: couldn't get matplotlib colormap:", e)

    # Set up experiment.
    if args.dim == 3:
        u_inflow = ("x[1]*(1-x[1])/2 * x[2]*(1-x[2])/2", "0", "0")
        mesh = UnitCubeMesh(args.grid, args.grid, args.grid)

        def sides_boundary(x, on_boundary):
            return on_boundary and (
                near(x[1], 0, 1e-5)
                or near(x[1], 1, 1e-5)
                or near(x[2], 0, 1e-5)
                or near(x[2], 1, 1e-5)
            )

    else:
        mesh = UnitSquareMesh(args.grid, args.grid)
        u_inflow = ("x[1]*(1-x[1])*2", "0")

        def sides_boundary(x, on_boundary):
            return on_boundary and (near(x[1], 0, 1e-5) or near(x[1], 1, 1e-5))

    u_noslip = zero(args.dim)
    eps2 = args.epsilon ** 2

    # Set quadrature params for Covering.
    quad_params = {"quadrature_degree": args.quad}
    domain = mesh.ufl_domain()
    set_default_covering_params(domain=domain, quad_params=quad_params)

    ex = Experiment(mesh, quad_params, g=0)

    # Set up boundary conditions.
    u_inflow = Expression(u_inflow, degree=2)

    def left_boundary(x, on_boundary):
        return on_boundary and near(x[0], 0, 1e-5)

    if args.robin:
        a = Constant(1)
        b = Constant(0.5)

        class LeftBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return left_boundary(x, on_boundary)

        class SidesBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return sides_boundary(x, on_boundary)

        # Create a function over the boundary mesh, initialized to 0, but set the Boundary to 1.
        markers = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
        LeftBoundary().mark(markers, 1)
        SidesBoundary().mark(markers, 2)
        dleft = ds(1, domain=mesh, subdomain_data=markers)
        dsides = ds(2, domain=mesh, subdomain_data=markers)

        rob_bcs = []
        rob_bcs.append((a, b, u_inflow, dleft))
        rob_bcs.append((a, b, u_noslip, dsides))

        ex.setBCs([], rob_bcs)
    else:
        u_noslip = Expression(("0",) * args.dim, degree=0)
        noslip = DirichletBC(ex.W.sub(0), u_noslip, sides_boundary)
        inflow = DirichletBC(ex.W.sub(0), u_inflow, left_boundary)
        dir_bcs = [noslip, inflow]
        ex.setBCs(dir_bcs)

    # Set up CR.
    p = Constant(args.ptrue, name="p")

    def cr_true_func(epsilon):
        e2 = tr(dot(epsilon, epsilon))
        mu = (e2 + eps2) ** ((p - 2) / 2)
        return mu * epsilon

    print("      True p: %g" % p)

    # Set up observer.
    if args.observer == "full":
        observer = lambda u: u
    else:

        class OutflowBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and near(x[0], 1, 1e-10)

        # Our obs are taken only on the outflow boundary.
        observer = SubdomainObserver(mesh, OutflowBoundary())
    # Our obs are also noisy.
    noisefunc = AdditiveRandomFunction(ex.W, seed=args.seed, std=args.noise_std)

    # Set up loss.
    loss = integral_loss

    # Run experiment to get base observations.
    with record_tape_block(name="Ground-truth_Experiment"):
        obs_u = ex.run(cr_true_func, ufl=True, disp=args.debug)
        obs = observer(noisefunc(obs_u))

    plt.figure()
    plot_solution(obs_u, filename=args.file)
    plt.title("Solution with p = %g (True)" % p)
    utils.saveplot()

    if args.mode == "ufl":
        run_ufl_cr(args, ex, obs, loss, observer)
    elif args.mode == "jax_plap":
        run_jax_cr(args, ex, obs, loss, observer, energy=False, plap=True)
    elif args.mode == "jax":
        run_jax_cr(args, ex, obs, loss, observer, energy=False)
    elif args.mode == "jax_energy":
        run_jax_cr(args, ex, obs, loss, observer, energy=True)
    elif args.mode == "network":
        run_jax_network_cr(args, ex, obs, loss, observer, energy=False)
    elif args.mode == "network_energy":
        run_jax_network_cr(args, ex, obs, loss, observer, energy=True)
    elif args.mode == "plap_network":
        run_jax_network_cr(args, ex, obs, loss, observer, energy=False, plap=True)
    elif args.mode == "plap_network_energy":
        run_jax_network_cr(args, ex, obs, loss, observer, energy=True, plap=True)
    elif args.mode == "torch_network":
        run_torch_network_cr(args, ex, obs, loss, observer)
    else:
        print("Internal error: invalid mode  '%s'" % args.mode)
        sys.exit(1)

    ############# Visualize everything I did. ##############
    if args.viz_tape:
        get_working_tape().visualise(launch_tensorboard=True, open_in_browser=True)

    if args.show:
        plt.show()
