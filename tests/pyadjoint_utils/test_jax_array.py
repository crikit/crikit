from pyadjoint_utils.jax_adjoint import *
from pyadjoint_utils import ReducedFunction, taylor_test, Control
import numpy as np
from jax import numpy as jnp


def test_array_add_pow():

    x = array([1.0, 2.0, 3.0, 4.0])
    y = array([2.0, 4.0, 5.0, -0.4])
    inputs = (x, y)
    val1 = x ** (x + y)
    val2 = val1 ** x
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y)])
    h = tuple(1e-3 * array(np.random.randn(*a.shape)) for a in inputs)
    v = tuple(array(np.random.randn(*a.shape)) for a in outputs)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9

    res = taylor_test(rf, inputs, h)
    assert res >= 1.9


def test_array_add_sub_mul():

    x = array([1.0, 2.0, 3.0, 4.0])
    y = array([2.0, 4.0, 5.0, -0.4])
    z = array([6.0, 8.7, 3.0, 8.6])
    inputs = (x, y, z)
    val1 = x * (x + y - z)
    val2 = val1 * x * y * z
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y), Control(z)])
    h = tuple(1e-3 * array(np.random.randn(*a.shape)) for a in inputs)
    v = tuple(array(np.random.randn(*a.shape)) for a in outputs)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9

    res = taylor_test(rf, inputs, h)
    assert res >= 1.9


def test_array_add_pow_const():

    x = array([1.0, 2.0, 3.0, 4.0])
    y = array([2.0, 4.0, 5.0, -0.4])
    inputs = (x, y)
    val1 = x ** (x + y)
    val2 = val1 ** 2
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y)])
    h = tuple(1e-3 * array(np.random.randn(*a.shape)) for a in inputs)
    v = tuple(array(np.random.randn(*a.shape)) for a in outputs)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9

    res = taylor_test(rf, inputs, h)
    assert res >= 1.9


def test_array_add_div_neg():

    x = array([1.0, 2.0, 3.0, 4.0])
    y = array([2.0, 4.0, 5.0, -0.4])
    inputs = (x, y)
    val1 = x / (x + y)
    outputs = (-val1,)
    rf = ReducedFunction(outputs, [Control(x), Control(y)])
    h = tuple(1e-3 * array(np.random.randn(*a.shape)) for a in inputs)
    v = tuple(array(np.random.randn(*a.shape)) for a in outputs)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9

    res = taylor_test(rf, inputs, h)
    assert res >= 1.9
