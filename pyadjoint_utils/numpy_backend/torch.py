import time
from .base import NumpyBackend


class TorchBackend(NumpyBackend):
    def __init__(self):
        import torch, functorch

        self.torch, self.functorch = torch, functorch
        from pyadjoint_utils.torch_adjoint import tensor

        self.tensor = tensor
        self.generator = None
        super().__init__(numpy=torch, name="torch")

    def vmap(self, f, in_axes=0, out_axes=0):
        return self.functorch.vmap(f, in_dims=in_axes, out_dims=out_axes)

    def jit(self, f, *args, static_argnums=None, **kwargs):
        return self.torch.jit.script(f, *args, **kwargs)

    def vjp(self, f, *inputs):
        outs, vjp = self.functorch.vjp(f, *inputs)

        def fvjp(cotans):
            return vjp(cotans, create_graph=True)

        return outs, fvjp

    def init_rng(self, seed=None):
        if seed is None:
            seed = int(time.time())

        self.generator = self.torch.random.manual_seed(seed)

    def randn(self, *args):
        return self.torch.randn(*args)

    def script_if_tracing(self, f):
        return self.torch.jit.script_if_tracing(f)

    def array(self, x):
        return self.tensor(x)

    def concatenate(self, tensors, dim=0):
        return self.torch.cat(tensors, dim=dim)

    def tensordot(self, *args, axes=0, **kwargs):
        return self.torch.tensordot(*args, dims=axes, **kwargs)

    def einsum(self, *args, optimize=None, **kwargs):
        return self.torch.einsum(*args, **kwargs)

    def index_update(self, x, idx, y):
        x[idx] = y
        return x
