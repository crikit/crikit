from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np

import sys

# This function handles plotting the flow data since FEniCS's plot function
# doesn't work for 3D plots. This function could be made more efficient.
def plot_flow(obs_w=None, filename="", obs_u=None, **kwargs):
    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

    import matplotlib.pyplot as plt
    import numpy as np

    if obs_w is not None and obs_u is None:
        obs_u, p = obs_w.split()

    if obs_u is not None:
        # Collect data from obs_u
        nx = 8
        ny = 8
        nz = 8

        xyz = [
            (x, y, z)
            for x in np.linspace(0, 1, nx)
            for y in np.linspace(0, 1, ny)
            for z in np.linspace(0, 1, nz)
        ]
        r = [obs_u(x, y, z) for x, y, z in xyz]

        xyz = np.array(xyz).reshape(-1, 3)
        r = np.array(r).reshape(-1, 3)

        if filename != "":
            print("Saving data to %s" % filename)
            np.savez(filename, xyz=xyz, r=r)
    else:
        # Read data from filename.
        npzfile = np.load(filename)

        xyz = npzfile["xyz"]
        r = npzfile["r"]

    C = np.linalg.norm(r, axis=1)
    C /= C.max()

    fig = plt.figure()
    ax = fig.gca(projection="3d")
    ax.quiver(
        xyz[:, 0],
        xyz[:, 1],
        xyz[:, 2],
        r[:, 0],
        r[:, 1],
        r[:, 2],
        C,
        length=3.5,
        normalize=False,
        **kwargs
    )

    return fig, ax


if __name__ == "__main__":
    filename = sys.argv[1]
    plot_flow(filename=filename)
    plt.show()
