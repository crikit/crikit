import torch

torch.set_default_tensor_type(torch.DoubleTensor)
from .tensor import (
    Tensor,
    tensor,
    get_default_dtype,
    set_default_dtype,
    to_adjfloat,
    to_numpy,
    to_cpu_numpy,
    to_torch,
)
from .torch import overload_torch
