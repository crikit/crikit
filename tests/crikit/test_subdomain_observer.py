from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.observer import SubdomainObserver
import pytest


def test_subdomain_observer():
    mesh = UnitSquareMesh(2, 2)

    class RightBoundary(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[0], 1, 1e-5) and on_boundary

    V = FunctionSpace(mesh, "CG", 1)
    expr = Expression("x[1]*x[0]", degree=1)
    c = Constant(1.4)
    u = project(c * expr, V)
    observer = SubdomainObserver(mesh, RightBoundary())
    v = observer(u)
    v *= c
    rf = ReducedFunctional(assemble(inner(v, v) * dx), Control(c))
    # assert taylor_test(rf, c, Constant(0.1)) >= 1.9

    assert taylor_test(rf, c, Constant(0.1)) >= 1.9
