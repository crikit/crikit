from crikit.cr.ufl import UFLFunctionSpace, CR_P_Laplacian, point_map
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint_utils import AdjFloat, ReducedFunctional


def test_cr_p_laplacian():

    mesh = UnitSquareMesh(2, 2)
    V = FunctionSpace(mesh, "P", 1)
    if crikit_fe_backend == "firedrake":
        x = SpatialCoordinate(V.mesh())
        v = interpolate(x[0] * x[0] + x[1] * x[1], V)
    else:
        v = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=1), V)
    g = grad(v)
    p = Constant(0.5)
    cr = CR_P_Laplacian(p)

    sigma = cr((v, g))

    energy = assemble(inner(grad(v), sigma) * dx)

    r = ReducedFunctional(energy, Control(p))

    e2 = r(1.0)


def test_point_map_decorator():

    mesh = UnitSquareMesh(2, 2)
    V = FunctionSpace(mesh, "P", 1)
    x = SpatialCoordinate(V.mesh())
    u = project(x[0] * x[0] + x[1] * x[1], V)
    g = grad(u)
    p = Constant(1.5)

    cr = CR_P_Laplacian(p)

    @point_map(((), (2,)), bare=False)
    def cr_dec(point):
        u, g = point
        return u * g * (inner(g, g) + 1e-12) ** ((p - 2) / 2.0)

    @point_map(((), (2,)), bare=True)
    def cr_dec_bare(u, g):
        return u * g * (inner(g, g) + 1e-12) ** ((p - 2) / 2.0)

    @point_map((2,), bare=False)
    def cr_dec_single_input(g):
        return g * (inner(g, g) + 1e-12) ** ((p - 2) / 2.0)

    # Check source.
    inputs = (u, g)
    assert cr_dec.source.is_point(inputs)
    assert cr_dec_bare.source.is_point(inputs)
    assert cr_dec_single_input.source.is_point(g)

    # Check target.
    assert cr_dec.target.is_point(g)
    assert cr_dec_bare.target.is_point(g)
    assert cr_dec_single_input.target.is_point(g)

    # Check output.
    sigma = u * cr(inputs)
    sigma_norm = assemble(inner(sigma, sigma) * dx)
    sigma1 = cr_dec(inputs)
    sigma2 = cr_dec_bare(inputs)
    assert assemble(inner(sigma - sigma1, sigma - sigma1) * dx) < 1e-10
    assert assemble(inner(sigma - sigma2, sigma - sigma2) * dx) < 1e-10

    sigma = cr(inputs)
    sigma_norm = assemble(inner(sigma, sigma) * dx)
    sigma3 = cr_dec_single_input(g)
    assert assemble(inner(sigma - sigma3, sigma - sigma3) * dx) < 1e-10


def test_ufl_function_space():
    mesh = UnitSquareMesh(2, 2)

    V = FunctionSpace(mesh, "P", 1)
    space = UFLFunctionSpace(V)
    p = space.point()

    assert space.shape() == ()
    assert space.is_point(p)

    V_vec = VectorFunctionSpace(mesh, "P", 1)
    space_vec = UFLFunctionSpace(V_vec)
    p_vec = space_vec.point()

    assert space_vec.shape() == (2,)
    assert space_vec.is_point(p_vec)

    assert not space.is_point(p_vec)
    assert not space_vec.is_point(p)
