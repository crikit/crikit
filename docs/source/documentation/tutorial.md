(crikit-tutorial)=

```{py:currentmodule} crikit.cr

```

# Tutorial

```{py:currentmodule} crikit.cr.ufl

```

Here I use the example CR {class}`CR_P_Laplacian`.

```{eval-rst}
.. testcode:: 

    from crikit.cr.ufl import CR_P_Laplacian
    from fenics import *
    mesh = UnitSquareMesh(4,4)
    V = FunctionSpace(mesh, 'P', 1)
    u = Function(V)
    cr = CR_P_Laplacian(Constant(2))
```

This CR is made to take a scalar function and its gradient as input, so we can run it like so:

```{eval-rst}
.. testcode:: 

    input_point = (u, grad(u))
    output_point = cr(input_point)
```

The `source` and `target` properties track the source and target Spaces of the point map.

```{eval-rst}
.. testcode:: 

    assert cr.source.is_point(input_point)
    assert cr.target.is_point(output_point)
```

Since this is a UFL CR, it can directly be used in a UFL form.

```{eval-rst}
.. testcode:: 

    v = TestFunction(V)
    F = (dot(output_point, grad(v)) - v)*dx
    u = solve(F == 0, u, DirichletBC(V, Constant(0), lambda x, on_boundary: on_boundary))
```

```{eval-rst}
.. testoutput:: 
   :hide:

   ...

```

Wrapping it all together into one code block:

```{eval-rst}
.. testcode:: 

    from crikit.cr.ufl import CR_P_Laplacian
    from fenics import *
    mesh = UnitSquareMesh(4,4)
    V = FunctionSpace(mesh, 'P', 1)
    u = Function(V)
    cr = CR_P_Laplacian(Constant(2))
    input_point = (u, grad(u))
    output_point = cr(input_point)
    v = TestFunction(V)
    F = (dot(output_point, grad(v)) - v)*dx
    u = solve(F == 0, u, DirichletBC(V, Constant(0), lambda x, on_boundary: on_boundary))
```

```{eval-rst}
.. testoutput:: 
   :hide:

   ...
```


