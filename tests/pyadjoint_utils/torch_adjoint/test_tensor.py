from pyadjoint_utils import taylor_test, ReducedFunction, Control
from pyadjoint_utils.torch_adjoint import Tensor, tensor
import torch


def test_array_add_pow():
    x = tensor([1.0, 2.0, 3.0, 4.0, 5.0])
    y = tensor(torch.rand(*x.shape))
    inputs = (x, y)
    val1 = x ** (x + y)
    val2 = val1 ** x
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y)])
    h = tuple(1e-3 * tensor(torch.rand(*a.shape)) for a in inputs)
    v = tuple(tensor(torch.rand(*a.shape)) for a in outputs)
    assert taylor_test(rf, inputs, h, v=v) >= 1.9


def test_array_add_pow_const():
    x = tensor([1.0, 2.0, 3.0, 4.0, 5.0])
    y = tensor(torch.rand(*x.shape))
    inputs = (x, y)
    val1 = x ** (x + y)
    val2 = val1 ** 2
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y)])
    h = tuple(1e-3 * tensor(torch.rand(*a.shape)) for a in inputs)
    v = tuple(tensor(torch.rand(*a.shape)) for a in outputs)
    assert taylor_test(rf, inputs, h, v=v) >= 1.9


def test_array_add_sub_mul():
    x = tensor([1.0, 2.0, 3.0, 4.0, 5.0])
    y = tensor(torch.rand(*x.shape))
    z = tensor(torch.rand(*x.shape))
    inputs = (x, y, z)
    val1 = x * (x + y - z)
    val2 = val1 * x * y * z
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y), Control(z)])
    h = tuple(1e-3 * tensor(torch.rand(*a.shape)) for a in inputs)
    v = tuple(tensor(torch.rand(*a.shape)) for a in outputs)
    assert taylor_test(rf, inputs, h, v=v) >= 1.9


def test_array_add_div_neg():
    x = tensor([1.0, 2.0, 3.0, 4.0, 5.0])
    y = tensor(torch.rand(*x.shape))
    inputs = (x, y)
    val1 = x / (x + y)
    val2 = -val1
    outputs = (val2,)
    rf = ReducedFunction(outputs, [Control(x), Control(y)])
    h = tuple(1e-3 * tensor(torch.rand(*a.shape)) for a in inputs)
    v = tuple(tensor(torch.rand(*a.shape)) for a in outputs)
    assert taylor_test(rf, inputs, h, v=v) >= 1.9
