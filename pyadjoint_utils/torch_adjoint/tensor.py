import torch
from pyadjoint.overloaded_type import (
    OverloadedType,
    register_overloaded_type,
    create_overloaded_object,
)
from pyadjoint.overloaded_function import overload_function
from pyadjoint_utils.block import Block
from pyadjoint.tape import get_working_tape, stop_annotating, annotate_tape
from functools import partial
from .blocks import AddBlock, SubBlock, PowBlock, MulBlock, DivBlock, NegBlock
from pyadjoint_utils.block_variable import BlockVariable
from pyadjoint_utils.convert import make_convert_block
from pyadjoint_utils.adjfloat import AdjFloat
from pyadjoint_utils.numpy_adjoint import ndarray
from pyadjoint.overloaded_type import create_overloaded_object as coo


def _reverse(block):
    def _reversed_block(operator, args, output):
        return block(operator, (args[1], args[0]), output)

    return _reversed_block


def _neg(x):
    return torch.neg(x)


def _add(x, y):
    return x.add(y)


def _mul(x, y):
    return x.mul(y)


def _truediv(x, y):
    return torch.div(x, y)


def _sub(x, y):
    return torch.sub(x, y)


def _pow(x, y):
    return torch.pow(x, y)


def _radd(x, y):
    return torch.add(y, x)


def _rmul(x, y):
    return torch.mul(y, x)


def _rtruediv(x, y):
    return y.div(x)


def _rsub(x, y):
    return torch.sub(y, x)


def _rpow(x, y):
    return torch.pow(y, x)


_torch_op_map = {
    "__neg__": (_neg, NegBlock),
    "__add__": (_add, AddBlock),
    "__mul__": (_mul, MulBlock),
    "__truediv__": (_truediv, DivBlock),
    "__rtruediv__": (_rtruediv, _reverse(DivBlock)),
    "__sub__": (_sub, SubBlock),
    "__pow__": (_pow, PowBlock),
    "__radd__": (_radd, _reverse(AddBlock)),
    "__rmul__": (_rmul, _reverse(MulBlock)),
    "__rsub__": (_rsub, _reverse(SubBlock)),
    "__rpow__": (_rpow, _reverse(PowBlock)),
}


def annotate_operator(orig_operator, nojit=True, op_map=_torch_op_map):
    """Decorates an operator like __add__, __sub__, etc.
    with JAX JIT compilation.
    """

    def is_unitary(op):
        # only one unary operator right now
        return op.__name__ == "__neg__"

    op, block_ctor = op_map[orig_operator.__name__]
    if not nojit:
        op = torch.jit.script(op)

    def annotated_operator(*args):
        # args[0] is always self
        output = tensor(op(*args))
        if annotate_tape():
            args = [
                arg
                if isinstance(arg, OverloadedType)
                else create_overloaded_object(arg)
                for arg in args
            ]
            block = block_ctor(op, args, output)
            tape = get_working_tape()
            tape.add_block(block)
            block.add_output(output.block_variable)

        return output

    annotated_operator.__name__ = orig_operator.__name__
    return annotated_operator


_crikit_torch_default_dtype = torch.float64
torch.set_default_dtype(_crikit_torch_default_dtype)


def set_default_dtype(d) -> None:
    """Sets the default data type for torch arrays used inside CRIKit. Defaults to torch.float64.

    :param dtype: The default data type to set
    :type dtype: torch.dtype
    """
    global _crikit_torch_default_dtype
    d = torch.dtype(d)
    torch.set_default_dtype(d)
    _crikit_torch_default_dtype = d


def get_default_dtype() -> torch.dtype:
    """Gets the default CRIKit torch dtype

    :returns: CRIKit default torch dtype
    :rtype: torch.dtype
    """

    global _crikit_torch_default_dtype
    return _crikit_torch_default_dtype


def tensor(t, *args, **kwargs):
    """Returns an overloaded {class}`Tensor` that inherits from {class}`torch.Tensor` but is an {class}`OverloadedType`.

    :return: The tensor represented by the inputs
    :rtype: Tensor
    """
    if isinstance(t, Tensor):
        return t
    if isinstance(t, torch.Tensor):
        return Tensor(t)
    return Tensor(torch.as_tensor(t, *args, **kwargs))


def from_scalar(*args, **kwargs):
    return tensor(*args, **kwargs).reshape(())


class Tensor(torch.Tensor, OverloadedType):
    _block_variable = None

    @staticmethod
    def __new__(cls, x, *args, **kwargs):
        return super().__new__(cls, x, *args, **kwargs)

    def __init__(self, x, block_var=None):
        super(OverloadedType, self).__init__()
        self._block_variable = block_var or BlockVariable(self)

    def to(self, *args, **kwargs):
        new = Tensor([])
        tmp = super(torch.Tensor, self).to(*args, **kwargs)
        new.data = tmp.data
        new.requires_grad = tmp.requires_grad
        new._block_variable = self._block_variable
        return new

    def clone(self, *args, **kwargs):
        return super(torch.Tensor, self).clone(*args, **kwargs)

    @property
    def block_variable(self):
        if self._block_variable is None:
            self._block_variable = BlockVariable(self)
        return self._block_variable

    @block_variable.setter
    def block_variable(self, bv):
        self._block_variable = bv

    @property
    def raw(self):
        """
        Return the {class}`Tensor` as a base {class}`torch.Tensor`.
        """
        return torch.tensor(self)

    def __repr__(self):
        return "pyadjoint_utils.torch_adjoint.Tensor(" + str(self.tolist()) + ")"

    @annotate_operator
    def __add__(self, other):
        pass

    @annotate_operator
    def __neg__(self):
        pass

    @annotate_operator
    def __truediv__(self, other):
        pass

    @annotate_operator
    def __rtruediv__(self, other):
        pass

    @annotate_operator
    def __radd__(self, other):
        return self.__add__(other)

    @annotate_operator
    def __sub__(self, other):
        pass

    @annotate_operator
    def __rsub__(self, other):
        pass

    @annotate_operator
    def __mul__(self, other):
        pass

    @annotate_operator
    def __pow__(self, other):
        pass

    @annotate_operator
    def __rpow__(self, other):
        pass

    def __iadd__(self, other):
        return NotImplemented

    def __imul__(self, other):
        return NotImplemented

    def __isub__(self, other):
        return NotImplemented

    @classmethod
    def _ad_init_object(cls, obj):
        return cls(obj)

    def _ad_create_checkpoint(self):
        return self.clone()

    def _ad_restore_at_checkpoint(self, checkpoint):
        return checkpoint

    def _ad_dim(self):
        return self.size

    def _ad_dot(self, other):
        return float(torch.inner(self.flatten(), other.flatten()))

    def _ad_mul(self, other):
        return self * other

    def _ad_add(self, other):
        return self + other

    def _ad_copy(self):
        return self.clone()

    def _ad_convert_type(self, value, **kwargs):
        if isinstance(value, Tensor):
            return value
        return Tensor(value)

    @staticmethod
    def _ad_assign_numpy(dst, src, offset):
        if hasattr(dst, "numel"):
            size = lambda x: x.numel()
        else:
            size = lambda x: x.size
        dst = torch.reshape(
            torch.from_numpy(src[offset : offset + size(dst)]), dst.shape
        )
        return dst, offset + size(dst)

    @staticmethod
    def _ad_to_list(self):
        return self.flatten().tolist()

    def __getitem__(self, item):
        annotate = annotate_tape()
        if annotate:
            block = TorchTensorSliceBlock(self, item)
            get_working_tape().add_block(block)

        with stop_annotating():
            out = super(torch.Tensor, self).__getitem__(item)

        if annotate:
            out = create_overloaded_object(out)
            block.add_output(out.create_block_variable())

        return out


register_overloaded_type(Tensor, torch.Tensor)


class TorchTensorSliceBlock(Block):
    def __init__(self, arr, item):
        super().__init__()
        self.add_dependency(arr)
        self.item = item

    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        adj_output = torch.zeros(inputs[0].shape)
        adj_output[self.item] = adj_inputs[0]
        return adj_output

    def recompute_component(self, inputs, block_variable, idx, prepared=None):
        return inputs[0][self.item]

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        return tlm_inputs[0][self.item]


ConvertTorchToAdjFloat = make_convert_block(AdjFloat, tensor, "ConvertTorchToAdjFloat")
ConvertAdjFloatToTorch = make_convert_block(tensor, AdjFloat, "ConvertAdjFloatToTorch")

to_adjfloat = overload_function(AdjFloat, ConvertTorchToAdjFloat)
adjfloat_to_torch = overload_function(tensor, ConvertAdjFloatToTorch)


def _backend_to_numpy_cpu(x):
    return coo(x.detach().cpu().numpy())


def _backend_to_numpy(x):
    return coo(x.detach().numpy())


def _backend_to_torch(x):
    return Tensor(torch.as_tensor(x))


ConvertTorchToNumpy = make_convert_block(
    _backend_to_numpy, _backend_to_torch, "ConvertTorchToNumpy"
)
ConvertNumpyToTorch = make_convert_block(
    _backend_to_torch, _backend_to_numpy, "ConvertNumpyToTorch"
)

ConvertTorchToCPUNumpy = make_convert_block(
    _backend_to_numpy_cpu, _backend_to_torch, "ConvertTorchToNumpy"
)
ConvertCPUNumpyToTorch = make_convert_block(
    _backend_to_torch, _backend_to_numpy_cpu, "ConvertNumpyToTorch"
)

to_numpy = overload_function(_backend_to_numpy, ConvertTorchToNumpy)
numpy_to_torch = overload_function(_backend_to_torch, ConvertNumpyToTorch)
to_cpu_numpy = overload_function(_backend_to_numpy_cpu, ConvertTorchToCPUNumpy)
cpu_numpy_to_torch = overload_function(_backend_to_torch, ConvertCPUNumpyToTorch)


def to_torch(x, to_cpu=False):
    """Converts either an {class}`AdjFloat` or a {class}`numpy.ndarray` to a torch {class}`Tensor`.
    :param x: The array or float to convert
    :type x: Union[AdjFloat, numpy.ndarray]
    :param to_cpu: If True, ensure the torch tensor lives on CPU, defaults to False
    :type to_cpu: bool
    :return: An overloaded torch `Tensor`
    :rtype: Tensor
    """
    if isinstance(x, AdjFloat):
        return adjfloat_to_torch(x)
    else:
        if to_cpu:
            return cpu_numpy_to_torch(x)
        else:
            return numpy_to_torch(x)
