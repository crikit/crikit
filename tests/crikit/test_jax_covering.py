from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.covering import get_composite_cr, set_default_covering_params
from jax import numpy as np
import numpy as onp
from pyadjoint_utils.jax_adjoint import array
from crikit.invariants import TensorType
from crikit.cr import CR, BlockCR, ReducedFunctionJAX, P_Laplacian
from crikit.cr.ufl import UFLExprSpace, UFLFunctionSpace, CR_UFL_Expr


def test_jax_covering_one_param():
    mesh = UnitSquareMesh(3, 3)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 1)
    U = TensorFunctionSpace(mesh, "P", 1)
    out_space = UFLFunctionSpace(U)
    p_np = array(2.5)

    def cr_func(scalar_invts, p):
        mu = np.mean(scalar_invts) ** ((p - 2.0) / 2.0)
        return np.array([mu, mu]).reshape((2,))

    # for UFL; the CR calculates these internally from the input and output types
    def scalar_invts(x):
        xx = x * x
        return tr(x), tr(xx)

    u_0 = project(Expression(("x[0]", "x[1]"), degree=1), V)
    g = sym(grad(u_0))
    p = AdjFloat(2.5)
    usi = scalar_invts(g)
    usi = (usi[0] + usi[1]) / 2
    expr = usi ** ((p - 2.0) / 2.0) * Identity(2) + usi ** ((p - 2.0) / 2.0) * g
    gg = UFLExprSpace(g).point()
    pos_map = {gg: 0}
    ufl_source = UFLExprSpace(g)

    ufl_cr = CR_UFL_Expr(ufl_source, expr, pos_map)

    input_type = TensorType.make_symmetric(2, 2)
    output_type = TensorType.make_symmetric(2, 2)
    cr = CR(
        output_type,
        (input_type,),
        cr_func,
        vmap=True,
        params=(p_np,),
    )

    quad_params = {"quadrature_degree": 2}
    set_default_covering_params(domain=domain, quad_params=quad_params)

    comp_cr = get_composite_cr(ufl_source, cr, out_space)
    sigma = comp_cr(g)
    # something that looks kinda-sorta like an L2 loss function
    rfv = assemble(inner(sigma, sigma) * dx)
    rfs = ReducedFunction(rfv, Control(p_np))

    u_comp_cr = get_composite_cr(ufl_source, ufl_cr, out_space)
    usigma = u_comp_cr(g)

    vec = lambda x: x.vector().vec().getArray()
    assert (
        errornorm(sigma, usigma) < 1.0e-7
    ), f"sigma is {vec(sigma)} and usigma is {vec(usigma)}"
    h = array(0.01)
    res = taylor_test(rfs, p_np, h)
    assert res >= 1.9

    # Test with u as the Control.
    rfs = ReducedFunction(rfv, Control(u_0))
    v = AdjFloat(1)
    h = Function(V)
    h.vector()[:] = onp.random.randn(len(h.vector()))
    u = project(Expression(("x[0]*x[0]", "x[1]*x[1]"), degree=1), V)

    # Test adjoint.
    res = taylor_test(rfs, u, h, v=v)
    assert res >= 1.9

    # Test TLM.
    res = taylor_test(rfs, u, h)
    assert res >= 1.9


def test_jax_covering_multi_param():
    mesh = UnitSquareMesh(3, 3)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 1)
    U = TensorFunctionSpace(mesh, "P", 1)
    out_space = UFLFunctionSpace(U)
    p_np = array(2.5)
    w_np = array(3.4)

    def multiparam_cr_func(scalar_invts, p, w):
        mup = np.mean(scalar_invts) ** ((p - 2.0) / 2.0)
        muw = np.mean(scalar_invts) ** ((w - 2.0) / 2.0)
        return np.array([mup, muw]).reshape((2,))

    u_0 = project(Expression(("x[0]", "x[1]"), degree=1), V)
    g = sym(grad(u_0))
    ufl_source = UFLExprSpace(g)

    input_type = TensorType.make_symmetric(2, 2)
    output_type = TensorType.make_symmetric(2, 2)

    multiparam_cr = CR(
        output_type,
        (input_type,),
        multiparam_cr_func,
        vmap=True,
        params=(p_np, w_np),
    )

    quad_params = {"quadrature_degree": 2}
    set_default_covering_params(domain=domain, quad_params=quad_params)
    comp_multicr = get_composite_cr(ufl_source, multiparam_cr, out_space)

    sigma = comp_multicr(g)
    val = assemble(inner(sigma, sigma) * dx)
    rf = ReducedFunction(val, [Control(p_np), Control(w_np)])
    h = (array(0.1), array(-0.1))
    res = taylor_test(rf, (p_np, w_np), h)
    assert res >= 1.9


def test_jax_covering_p_laplacian():
    mesh = UnitSquareMesh(3, 3)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 1)
    out_space = UFLFunctionSpace(V)
    a_np = array(1.3)
    p_np = array(3.4)

    u_0 = project(Expression(("x[0]", "x[1]"), degree=1), V)
    g = sym(grad(u_0))
    ufl_source = UFLExprSpace(g)

    input_type = TensorType.make_symmetric(2, 2)
    output_type = TensorType.make_symmetric(2, 2)

    multiparam_cr = P_Laplacian(a_np, p_np, spatial_dims=2, eps2=1.0e-6)

    quad_params = {"quadrature_degree": 2}
    set_default_covering_params(domain=domain, quad_params=quad_params)
    comp_multicr = get_composite_cr(ufl_source, multiparam_cr, out_space)

    sigma = comp_multicr(g)
    val = assemble(inner(sigma, sigma) * dx)
    rf = ReducedFunction(val, [Control(a_np), Control(p_np)])
    h = (array(0.1), array(-0.1))
    res = taylor_test(rf, (a_np, p_np), h)
    assert res >= 1.9


def test_jax_covering_one_param_block_cr():
    mesh = UnitSquareMesh(3, 3)
    domain = mesh.ufl_domain()
    V = VectorFunctionSpace(mesh, "P", 1)
    U = TensorFunctionSpace(mesh, "P", 1)
    out_space = UFLFunctionSpace(U)
    p_np = array(2.5)

    def cr_func(scalar_invts, p):
        # same number of scalar and form invariants for this CR
        return p * scalar_invts

    u_0 = project(Expression(("x[0]", "x[1]"), degree=1), V)
    g = sym(grad(u_0))
    p_np = array(1.5)
    ufl_source = UFLExprSpace(g)

    input_type = TensorType.make_symmetric(2, 2)
    output_type = TensorType.make_symmetric(2, 2)
    cr = BlockCR(
        output_type,
        (input_type,),
        cr_func,
        params=(p_np,),
    )

    quad_params = {"quadrature_degree": 2}
    set_default_covering_params(domain=domain, quad_params=quad_params)

    comp_cr = get_composite_cr(ufl_source, cr, out_space)
    sigma = comp_cr(g)
    # something that looks kinda-sorta like an L2 loss function
    rfv = assemble(inner(sigma, sigma) * dx)
    rfs = ReducedFunction(rfv, Control(p_np))

    h = array(0.01)
    res = taylor_test(rfs, p_np, h)
    assert res >= 1.9

    # Test with u as the Control.
    rfs = ReducedFunction(rfv, Control(u_0))
    v = AdjFloat(1)
    h = Function(V)
    h.vector()[:] = onp.random.randn(len(h.vector()))
    u = project(Expression(("x[0]*x[0]", "x[1]*x[1]"), degree=1), V)

    # Test adjoint.
    res = taylor_test(rfs, u, h, v=v)
    assert res >= 1.9

    # Test TLM.
    res = taylor_test(rfs, u, h)
    assert res >= 1.9


if __name__ == "__main__":
    import cProfile, pstats

    profiler = cProfile.Profile()
    profiler.enable()
    test_jax_covering_multi_param()
    profiler.disable()
    stats = pstats.Stats(profiler).sort_stats("tottime")
    stats.print_stats()
