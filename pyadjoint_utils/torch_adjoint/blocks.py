from pyadjoint_utils.block import Block
import torch


class ArrayOperatorBlock(Block):
    def __init__(self, operator, args, output):
        super().__init__()
        self._operator = operator
        self._args = args
        for dep in args:
            # print('dep is ', dep, 'with type', type(dep))
            # print('it has dir', dir(dep))
            self.add_dependency(dep)

    def recompute_component(self, inputs, block_variable, idx, prepared):
        return self._operator(*inputs)


class AddBlock(ArrayOperatorBlock):
    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        return adj_inputs[0]

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        x_dot, y_dot = tlm_inputs
        return sum([x if x is not None else 0 for x in [x_dot, y_dot]])

    def evaluate_hessian_component(
        self,
        inputs,
        hessian_inputs,
        adj_inputs,
        block_variable,
        idx,
        relevant_dependencies,
        prepared=None,
    ):
        return hessian_inputs[0]


class SubBlock(ArrayOperatorBlock):
    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        if idx == 0:
            return adj_inputs[0]
        return -adj_inputs[0]

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        x_dot, y_dot = tlm_inputs
        output = 0
        if x_dot is not None:
            output = output + x_dot
        if y_dot is not None:
            output = output - y_dot

        return output


class MulBlock(ArrayOperatorBlock):
    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        other_idx = 0 if idx == 1 else 1
        return self._operator(adj_inputs[0], inputs[other_idx])

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        x_dot, y_dot = tlm_inputs
        output = 0
        if x_dot is not None:
            output = output + self._operator(inputs[1], x_dot)
        if y_dot is not None:
            output = output + self._operator(inputs[0], y_dot)
        return output


class DivBlock(ArrayOperatorBlock):
    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        z_bar = adj_inputs[0]
        if idx == 0:
            return z_bar / inputs[1]
        else:
            return -z_bar * inputs[0] / (inputs[1] ** 2)

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):

        x, y = inputs
        x_dot, y_dot = tlm_inputs

        def _tlm_arg_0(x, y, x_dot):
            return x_dot / y

        def _tlm_arg_1(x, y, y_dot):
            return -y_dot * (x / (y ** 2))

        tlm = []

        if x_dot is not None:
            tlm.append(_tlm_arg_0(x, y, x_dot))
        if y_dot is not None:
            tlm.append(_tlm_arg_1(x, y, y_dot))

        return sum(tlm)


class NegBlock(ArrayOperatorBlock):
    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        return -adj_inputs[0]

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):
        return -tlm_inputs[0]


class PowBlock(ArrayOperatorBlock):
    def evaluate_adj_component(
        self, inputs, adj_inputs, block_variable, idx, prepared=None
    ):
        x, y = inputs
        z_bar = adj_inputs[0]

        def _arg_0_vjp(x, y, z_bar):
            # vjp w.r.t. x; derivative is y * x ** (y - 1)
            return z_bar * y * (x ** (y - 1))

        def _arg_1_vjp(x, y, z_bar):
            # vjp w.r.t. y; derivative is log(x) * x ** y
            return z_bar * torch.log(x) * (x ** y)

        if idx == 0:
            return _arg_0_vjp(x, y, z_bar)
        else:
            return _arg_1_vjp(x, y, z_bar)

    def evaluate_tlm_component(
        self, inputs, tlm_inputs, block_variable, idx, prepared=None
    ):

        x, y = inputs
        x_dot, y_dot = tlm_inputs

        def _tlm_arg_0(x, y, x_dot):
            return x_dot * y * (x ** (y - 1))

        def _tlm_arg_1(x, y, y_dot):
            return y_dot * torch.log(x) * (x ** y)

        tlm = []

        if x_dot is not None:
            tlm.append(_tlm_arg_0(x, y, x_dot))
        if y_dot is not None:
            tlm.append(_tlm_arg_1(x, y, y_dot))
        return sum(tlm)
