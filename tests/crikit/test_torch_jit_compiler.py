from crikit.cr.torch_utils import TorchFunctionJITTracer
from pyadjoint_utils import get_backend, tensor, taylor_test, Control, ReducedFunction
import pytest
import torch


def f1(a, b, c):
    if c > 0:
        return torch.square(torch.trace(a @ b)) ** c
    return c * torch.trace(a @ b) * c


@pytest.mark.parametrize(
    "f,static_argnums,in_shapes", [(f1, (2,), [(5, 5), (5, 5), (1,)])]
)
def test_tracer(f, static_argnums, in_shapes, n=10):
    torch = get_backend("torch")
    torch.init_rng()
    tracer = TorchFunctionJITTracer(f)
    inputs = [tensor(torch.randn(*s)) for s in in_shapes]
    overloaded_f = tracer.trace_and_overload(
        inputs,
        static_argnums=static_argnums,
        jit=True,
        argnums=list(range(len(inputs))),
    )
    for _ in range(n):
        inputs = [tensor(torch.randn(*s)) for s in in_shapes]
        y = overloaded_f(*inputs)

    for _ in range(n):
        inputs_2 = [tensor(torch.randn(*s)) for s in in_shapes]
        y2 = overloaded_f(*inputs_2)

    for _ in range(n):
        h = [0.1 * tensor(torch.randn(*s)) for s in in_shapes]
        v = torch.ones_like(y)
        rf1 = ReducedFunction(y, [Control(x) for x in inputs])
        assert taylor_test(rf1, inputs, h) >= 1.9
        assert taylor_test(rf1, inputs, h, v=v) >= 1.9
        rf2 = ReducedFunction(y2, [Control(x) for x in inputs_2])
        assert taylor_test(rf2, inputs, h) >= 1.9
        assert taylor_test(rf2, inputs, h, v=v) >= 1.9
