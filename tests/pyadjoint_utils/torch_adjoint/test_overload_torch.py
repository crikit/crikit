from pyadjoint_utils import taylor_test, ReducedFunction, Control, Enlist
from pyadjoint_utils.torch_adjoint import (
    Tensor,
    tensor,
    overload_torch,
    to_adjfloat,
    to_torch,
)
import torch
from pyadjoint_utils import AdjFloat
from functools import partial
from pyadjoint_utils.identity import JacobianIdentity
from crikit.cr.torch_utils import TorchFunctionJITTracer

try:
    import functorch
except:
    import logging

    logging.warning("functorch import not found!")

    class functorch:
        accursed_unutterable_class_variable: int = 1

        @staticmethod
        def vmap(f, *args, **kwargs):
            return f


import pytest
import torch.autograd.functional as ad


def test_univariate_jvp():
    arr = tensor(2.0)

    @overload_torch
    def f(x):
        return x ** 4

    y = f(arr)

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (tensor(1.0),)
    res = taylor_test(rf, inputs, h)  # ,v=v)
    assert res >= 1.9


def test_univariate_vjp():
    arr = tensor(2.0)

    @overload_torch
    def f(x):
        return x ** 4

    y = f(arr)

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (tensor(1.0),)
    v = (tensor(1.0),)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9


def test_to_adjfloat_jvp():

    arr = tensor(1.0)

    @overload_torch
    def f(x):
        return x ** 4

    y = to_adjfloat(f(arr))

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (tensor(1.0),)
    v = (AdjFloat(1.0),)
    res = taylor_test(rf, inputs, h)
    assert res >= 1.9


def test_to_adjfloat_vjp():

    arr = tensor(2.0)

    @overload_torch
    def f(x):
        return x ** 4

    y = to_adjfloat(f(arr))

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (tensor(1.0),)
    v = (AdjFloat(1.0),)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9


def test_from_adjfloat_jvp():

    arr = AdjFloat(2.0)

    @overload_torch
    def f(x):
        return x ** 4

    y = f(to_torch(arr))

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (AdjFloat(1.0),)
    v = (tensor(1.0),)
    res = taylor_test(rf, inputs, h)
    assert res >= 1.9


def test_from_adjfloat_vjp():

    arr = AdjFloat(2.0)

    @overload_torch
    def f(x):
        return x ** 4

    y = f(to_torch(arr))

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (AdjFloat(1.0),)
    v = (tensor(1.0),)
    res = taylor_test(rf, inputs, h, v=v)
    assert res >= 1.9


def test_single_input_jvp():
    arr = tensor([2.0, 2.0])

    @overload_torch
    def f(x):
        return torch.sum(x ** 2)

    assert torch.allclose(f(arr), torch.tensor(8.0))

    out = f(arr)
    gf = ReducedFunction(out, Control(arr))
    inputs = [arr]
    outputs = [out]
    h = torch.rand(*arr.shape)
    v = tensor(1.0).reshape(())
    assert taylor_test(gf, inputs, h) >= 1.9


def test_single_input_vjp():
    arr = tensor([2.0, 2.0])

    @overload_torch
    def f(x):
        return x ** 2

    @overload_torch
    def g(x):
        return torch.sum(x)

    assert torch.allclose(f(arr), torch.tensor([4.0, 4.0]))

    y = f(arr)
    out = g(y)
    gf = ReducedFunction(out, Control(arr))
    inputs = [arr]
    outputs = [out]
    h = tuple(torch.rand(*x.shape) for x in inputs)
    v = tensor(1.0).reshape(())
    assert taylor_test(gf, inputs, h, v=v) >= 1.9


@partial(overload_torch, argnums=(0, 1))
def one_output(A, B):
    return torch.exp(torch.trace(A @ B))


@partial(overload_torch, argnums=(0, 1))
def two_outputs(A, B):
    return torch.exp(torch.trace(A @ B)), torch.exp(torch.trace(A @ A @ B @ B))


@partial(overload_torch, argnums=(0, 1))
def two_matrix_outputs(A, B):
    return torch.exp(torch.trace(A @ B)) * A, torch.exp(torch.trace(A @ A @ B @ B)) * B


class OneOutputClass:
    __name__ = "OneOutputClass"
    x: float = 3.0

    def __call__(self, A, B):
        return torch.exp(self.x * torch.trace(A @ B))


class TracedOneOutput:
    def __init__(self):
        self.tracer = TorchFunctionJITTracer(OneOutputClass())
        self.traced = None

    def __call__(self, A, B):
        if self.traced is None:
            self.traced = self.tracer.trace_and_overload(A, B, static_argnums=(0,))

        return self.traced(A, B)


@partial(
    overload_torch,
    argnums=(0, 1),
    pointwise=(True, False),
    out_pointwise=(True, True, True),
    jit=False,
)
@partial(functorch.vmap, in_dims=(0, None), out_dims=(0, 0, 0))
def three_outputs_pointwise(v, B):
    one = torch.exp(torch.trace(B)) * B
    two = torch.outer(B @ v, v)
    three = torch.dot(v, B @ v) * two
    four = torch.exp(torch.trace(B @ B)) * B
    return two, three, four


if hasattr(functorch, "accursed_unutterable_class_variable"):

    def three_outputs_pointwise(v, B):
        return v, B, B


@pytest.mark.parametrize(
    "func",
    [
        one_output,
        two_outputs,
        two_matrix_outputs,
        three_outputs_pointwise,
        # TracedOneOutput()
        # one_output_caller -- for some reason, self instance is not being passed to __call__ in the TorchBlock (so it fails)
    ],
)
def test_two_input_jvp_vjp_jacobian(func):
    X = tensor(torch.rand(3, 3))
    Y = tensor(torch.rand(3, 3))
    inputs = (X, Y)

    controls = tuple(Control(x) for x in inputs)
    outputs = Enlist(func(*inputs))
    rf = ReducedFunction(outputs, controls)
    h = tuple(1e-3 * torch.rand(*x.shape) for x in inputs)
    v = tuple(
        tensor(torch.rand(*a.shape)) if len(a.shape) > 0 else tensor(1.0).reshape(())
        for a in outputs
    )

    # test Jacobian
    J = rf.jac_matrix()
    jac_tuple = ad.jacobian(func, tuple(inputs))
    # J = J[0]
    if not outputs.listed:
        jac_tuple = (jac_tuple,)

    assert len(J) == len(jac_tuple)
    for i, (xs, ys) in enumerate(zip(J, jac_tuple)):
        for j, (x, y) in enumerate(zip(xs, ys)):
            x = x.squeeze()
            y = y.squeeze()
            if len(y.shape) > len(x.shape):
                y = torch.sum(y, dim=len(outputs[i].shape))
            elif len(x.shape) > len(y.shape):
                x = torch.sum(x, dim=len(outputs[i].shape))
            assert torch.allclose(
                x, y, atol=1e-5
            ), f"{x-y} with size {(x-y).size()} (from {x.size()} and {y.size()}) is not zero!"

    # test vjp
    assert taylor_test(rf, inputs, h, v=v) >= 1.9
    # test jvp
    assert taylor_test(rf, inputs, h) >= 1.9
