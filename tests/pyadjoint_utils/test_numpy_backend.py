from pyadjoint_utils.numpy_backend import get_backend
import pytest

# notation for test functions is f_<num inputs>_<non-pointwise argnums>


def f_1_0(x):
    return x @ x


def f_2_1(x, theta):
    return theta @ x @ theta.T


def f_5_13(x, theta, y, phi, z):
    return x + theta @ y + phi @ z


@pytest.mark.parametrize(
    "f,point_input_shapes,pointwise",
    [
        (f_1_0, [(3, 3)], [True]),
        (f_2_1, [(3, 3), (3, 3)], [True, False]),
        (
            f_5_13,
            [(5,), (5, 3), (3,), (5, 10), (10,)],
            [True, False, True, False, True],
        ),
    ],
)
def test_numpy_vmap(f, point_input_shapes, pointwise, n=100):
    # assumes that the vmap() for the JAX backend is correct
    # since jax.vmap() is what the numpy backend is trying to emulate
    # with its vmap()
    jax = get_backend("jax")
    jax.init_rng()
    numpy = get_backend("numpy")
    if pointwise is None:
        pointwise = [True] * len(point_input_shapes)
    # test the jax rng but use numpy array inputs
    inputs = [
        numpy.asarray(jax.randn(n, *shp)) if pw else jax.randn(*shp)
        for shp, pw in zip(point_input_shapes, pointwise)
    ]
    in_axes = [0 if pw else None for pw in pointwise]
    jax_outputs = jax.vmap(f, in_axes=in_axes)(*inputs)
    assert jax.allclose(jax_outputs, numpy.vmap(f, in_axes=in_axes)(*inputs))
