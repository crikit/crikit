from crikit import *
from crikit.loss import SlicedWassersteinDistance
from jax import numpy as jnp
from jax import random
import pyadjoint


def test_sliced_wasserstein():
    mesh = UnitSquareMesh(2, 2)
    set_default_covering_params(
        domain=mesh.ufl_domain(), quad_params={"quadrature_degree": 2}
    )
    V = VectorFunctionSpace(mesh, "CG", 1)
    u = project(Expression(("x[1]", "pow(x[0], 2)"), degree=1), V)
    c = Constant(10.0)
    w = c * c * project(Expression(("sin(x[0])", "pow(x[1], 3)"), degree=1), V)
    key = random.PRNGKey(0)
    lossfun = SlicedWassersteinDistance(V, 3, key)
    err = lossfun(w, u)
    rf = ReducedFunction(err, Control(c))
    h = Constant(
        1.0
    )  # 0.1# * #project(Expression(("x[1]", "pow(x[0], 3)"), degree=1), V)
    assert taylor_test(rf, c, h) >= 1.9


def test_sliced_wasserstein_p_2():
    mesh = UnitSquareMesh(2, 2)
    set_default_covering_params(
        domain=mesh.ufl_domain(), quad_params={"quadrature_degree": 2}
    )
    V = VectorFunctionSpace(mesh, "CG", 1)
    u = project(Expression(("x[1]", "pow(x[0], 2)"), degree=1), V)
    c = Constant(10.0)
    w = c * c * project(Expression(("sin(x[0])", "pow(x[1], 3)"), degree=1), V)
    key = random.PRNGKey(0)
    lossfun = SlicedWassersteinDistance(V, 3, key, p=2)
    err = lossfun(w, u)
    rf = ReducedFunction(err, Control(c))
    h = Constant(
        1.0
    )  # 0.1# * #project(Expression(("x[1]", "pow(x[0], 3)"), degree=1), V)
    v = array(1.0)
    assert taylor_test(rf, c, h, v=v) >= 1.9


if __name__ == "__main__":
    test_sliced_wasserstein()
