# p-Stokes Examples

This directory contains examples that learn a CR for the p-Stokes problem, which is the nonlinear version of the Stokes flow problem shown [here](https://fenicsproject.org/olddocs/dolfin/2016.2.0/python/demo/documented/stokes-taylor-hood/python/documentation.html).

We recommend beginners start with the Jupyter notebook, which runs through a simplified, minimal version of the problem shown in `p-stokes.py` that is organized for maximum readability. `p-stokes.py` shows more complicated examples, such as using neural networks created with [Flax](https://flax.readthedocs.io/en/latest/) to infer the CR, as well as more sophisticated plotting and visualization.

If you want to change the settings that the nonlinear solver uses, take a look at the `.petscrc` in this directory, and the various available options for the nonlinear solver that you can add to the `.petscrc` (which are the various PETSc `SNES` options that you can see by putting `-h` in your `.petscrc` and filtering the output for options that start with `-snes_`).