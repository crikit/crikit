from crikit.deprecated.invariants import (
    TensorDefinition as TD,
    register_invariant,
    concatenate_tensors,
)
from pyadjoint.overloaded_type import create_overloaded_object as coo
import autograd.numpy as np

# Zheng: Theory of representations for tensor functions. 1994.

levi_civita = np.zeros((3, 3, 3))
levi_civita[0, 1, 2] = levi_civita[1, 2, 0] = levi_civita[2, 0, 1] = 1
levi_civita[0, 2, 1] = levi_civita[1, 0, 2] = levi_civita[2, 1, 0] = -1


def scalar_triple_product(v, u, w):
    return np.einsum("ijk, ...i, ...j, ...k -> ...", levi_civita, v, u, w)


def scalar_vector_tensor_product(v, G):
    return np.einsum("ijk, ...i, ...jk -> ...", levi_civita, v, G)


def stacked_matvec(G, v):
    return np.einsum("...ij, ...j -> ...i", G, v)


def stacked_dot(v, u):
    return np.einsum("...i, ...i -> ...", v, u)


def stacked_outer_product(v, u):
    return np.einsum("...i, ...j -> ...ij", v, u)


def get_symmetric(G):
    return G + np.transpose(G, (0, 2, 1))


def get_antisymmetric(G):
    return G - np.transpose(G, (0, 2, 1))


#### Table 1 ####
#################
@register_invariant(3, TD(2, "s"), num_invariants=3)
def SO3_A(A):
    A2 = A @ A
    A3 = A2 @ A

    a = np.trace(A, axis1=1, axis2=2)
    b = np.trace(A2, axis1=1, axis2=2)
    c = np.trace(A3, axis1=1, axis2=2)
    return np.column_stack((a, b, c))


@register_invariant(3, TD(2, "s"), TD(2, "s"), num_invariants=4)
def SO3_AB(A, B):
    AB = A @ B
    AB2 = AB @ B
    A2B = A @ AB
    A2B2 = A2B @ B
    mats = (AB, A2B, AB2, A2B2)
    invs = (np.trace(m, axis1=1, axis2=2) for m in mats)
    return np.column_stack(invs)


@register_invariant(3, TD(2, "s"), TD(2, "s"), TD(2, "s"), num_invariants=1)
def SO3_ABC(A, B, C):
    ABC = A @ B @ C
    return np.trace(ABC, axis1=1, axis2=2)


@register_invariant(3, TD(2, "a"), num_invariants=1)
def SO3_W(W):
    c = np.trace(W @ W, axis1=1, axis2=2)
    return c


@register_invariant(3, TD(2, "s"), TD(2, "a"), num_invariants=3)
def SO3_AW(A, W):
    AW = A @ W
    AW2 = AW @ W
    A2W2 = A @ AW2
    A2W2AW = A2W2 @ AW
    mats = (AW2, A2W2, A2W2AW)
    invs = (np.trace(m, axis1=1, axis2=2) for m in mats)
    return np.column_stack(invs)


@register_invariant(3, TD(2, "s"), TD(2, "s"), TD(2, "a"), num_invariants=4)
def SO3_ABW(A, B, W):
    BW = B @ W
    ABW = A @ BW
    A2BW = A @ ABW
    AB2W = A @ B @ BW
    AW2BW = A @ W @ W @ BW

    mats = (ABW, A2BW, AB2W, AW2BW)
    invs = (np.trace(m, axis1=1, axis2=2) for m in mats)
    return np.column_stack(invs)


@register_invariant(3, TD(2, "a"), TD(2, "a"), num_invariants=1)
def SO3_WV(W, V):
    c = np.trace(W @ V, axis1=1, axis2=2)
    return c


@register_invariant(3, TD(2, "s"), TD(2, "a"), TD(2, "a"), num_invariants=3)
def SO3_AWV(A, W, V):
    AW = A @ W
    AWV = AW @ V
    AW2V = AW @ W @ V
    AWV2 = AWV @ V

    mats = (AWV, AW2V, AWV2)
    invs = (np.trace(m, axis1=1, axis2=2) for m in mats)
    return np.column_stack(invs)


@register_invariant(3, TD(2, "a"), TD(2, "a"), TD(2, "a"), num_invariants=1)
def SO3_WVU(W, V, U):
    c = np.trace(W @ V @ U, axis1=1, axis2=2)
    return c


@register_invariant(3, TD(1, None), num_invariants=1)
def SO3_v(v):
    return stacked_dot(v, v)


@register_invariant(3, TD(2, "s"), TD(1, None), num_invariants=3)
def SO3_Av(A, v):
    Av = stacked_matvec(A, v)
    A2v = stacked_matvec(A, Av)

    a = stacked_dot(v, Av)
    b = stacked_dot(Av, Av)
    c = scalar_triple_product(v, Av, A2v)
    return np.column_stack((a, b, c))


@register_invariant(3, TD(2, "s"), TD(2, "s"), TD(1, None), num_invariants=4)
def SO3_ABv(A, B, v):
    AB = A @ B
    A2B = A @ AB
    AB2 = AB @ B
    Av = stacked_matvec(A, v)
    Bv = stacked_matvec(B, v)

    a = scalar_vector_tensor_product(v, AB)
    b = scalar_vector_tensor_product(v, A2B)
    c = scalar_vector_tensor_product(v, AB2)
    d = scalar_triple_product(v, Av, Bv)
    return np.column_stack((a, b, c, d))


@register_invariant(3, TD(2, "a"), TD(1, None), num_invariants=1)
def SO3_Wv(W, v):
    return scalar_vector_tensor_product(v, W)


@register_invariant(3, TD(2, "s"), TD(2, "a"), TD(1, None), num_invariants=3)
def SO3_AWv(A, W, v):
    AW = A @ W
    AW2 = AW @ W
    AWv = stacked_matvec(AW, v)

    a = stacked_dot(v, AWv)
    b = scalar_vector_tensor_product(v, AW)
    c = scalar_vector_tensor_product(v, AW2)
    return np.column_stack((a, b, c))


@register_invariant(3, TD(2, "a"), TD(2, "a"), TD(1, None), num_invariants=1)
def SO3_WVv(W, V, v):
    return scalar_vector_tensor_product(v, W @ V)


@register_invariant(3, TD(1, None), TD(1, None), num_invariants=1)
def SO3_vu(v, u):
    return stacked_dot(v, u)


@register_invariant(3, TD(2, "s"), TD(1, None), TD(1, None), num_invariants=3)
def SO3_Avu(A, v, u):
    Au = stacked_matvec(A, u)
    Av = stacked_matvec(A, v)
    a = stacked_dot(v, u)
    b = scalar_triple_product(v, u, Av)
    c = scalar_triple_product(v, u, Au)
    return np.column_stack((a, b, c))


# Note: copied from Isotropy column, but  I'm not sure that's what the table means.
@register_invariant(
    3, TD(2, "s"), TD(2, "s"), TD(1, None), TD(1, None), num_invariants=1
)
def SO3_ABvu(A, B, v, u):
    ABu = stacked_matvec(A, stacked_matvec(B, u))
    BAu = stacked_matvec(B, stacked_matvec(A, u))
    return stacked_dot(v, ABu - BAu)


@register_invariant(3, TD(2, "a"), TD(1, None), TD(1, None), num_invariants=1)
def SO3_Wvu(W, v, u):
    return stacked_dot(v, stacked_matvec(W, u))


# Note: copied from Isotropy column, but  I'm not sure that's what the table means.
@register_invariant(
    3, TD(2, "s"), TD(2, "a"), TD(1, None), TD(1, None), num_invariants=1
)
def SO3_AWvu(A, W, v, u):
    AWu = stacked_matvec(A, stacked_matvec(W, u))
    WAu = stacked_matvec(W, stacked_matvec(A, u))
    return stacked_dot(v, AWu + WAu)


# Note: copied from Isotropy column, but  I'm not sure that's what the table means.
@register_invariant(
    3, TD(2, "a"), TD(2, "a"), TD(1, None), TD(1, None), num_invariants=1
)
def SO3_WVvu(W, V, v, u):
    WVu = stacked_matvec(W, stacked_matvec(V, u))
    VWu = stacked_matvec(V, stacked_matvec(W, u))
    return stacked_dot(v, WVu - VWu)


@register_invariant(3, TD(1, None), TD(1, None), TD(1, None), num_invariants=1)
def SO3_vuw(v, u, w):
    return scalar_triple_product(v, u, w)


#### End Table 1 ####
#####################


#### Table 2 ####
#################
@register_invariant(3, num_invariants=1, output_def=TD(2, "s"))
def SO3_2s_():
    return coo(np.eye(3)[None, ...])


@register_invariant(3, TD(2, "s"), num_invariants=2, output_def=TD(2, "s"))
def SO3_2s_A(A):
    A2 = A @ A
    return concatenate_tensors(A, A2, ndim=A.ndim + 1)


@register_invariant(3, TD(2, "s"), TD(2, "s"), num_invariants=3, output_def=TD(2, "s"))
def SO3_2s_AB(A, B):
    AB = A @ B
    A2B = A @ AB
    AB2 = AB @ B
    outputs = [get_symmetric(T) for T in [AB, A2B, AB2]]
    return concatenate_tensors(*outputs, ndim=A.ndim + 1)


@register_invariant(3, TD(2, "a"), num_invariants=1, output_def=TD(2, "s"))
def SO3_2s_W(W):
    return W @ W


@register_invariant(3, TD(2, "s"), TD(2, "a"), num_invariants=4, output_def=TD(2, "s"))
def SO3_2s_AW(A, W):
    AW = A @ W
    AW2 = AW @ W
    WAW2 = W @ AW2
    A2W = A @ AW
    outputs = [get_symmetric(T) for T in [AW, AW2, A2W, WAW2]]
    return concatenate_tensors(*outputs, ndim=A.ndim + 1)


@register_invariant(3, TD(2, "a"), TD(2, "a"), num_invariants=3, output_def=TD(2, "s"))
def SO3_2s_WV(W, V):
    WV = W @ V
    W2V = W @ WV
    WV2 = WV @ V
    outputs = [get_symmetric(T) for T in [WV, W2V, WV2]]
    return concatenate_tensors(*outputs, ndim=W.ndim + 1)


@register_invariant(3, TD(1, None), num_invariants=1, output_def=TD(2, "s"))
def SO3_2s_v(v):
    return stacked_outer_product(v, v)


@register_invariant(3, TD(2, "s"), TD(1, None), num_invariants=2, output_def=TD(2, "s"))
def O3_2s_Av(A, v):
    Av = stacked_matvec(A, v)
    A2v = stacked_matvec(A, Av)
    v_Av = get_symmetric(stacked_outer_product(v, Av))
    v_A2v = get_symmetric(stacked_outer_product(v, A2v))
    return concatenate_tensors(v_Av, v_A2v, ndim=A.ndim + 1)


@register_invariant(3, TD(2, "a"), TD(1, None), num_invariants=3, output_def=TD(2, "s"))
def O3_2s_Wv(W, v):
    Wv = stacked_matvec(W, v)
    W2v = stacked_matvec(W, Wv)
    v_Wv = get_symmetric(stacked_outer_product(v, Wv))
    Wv_Wv = stacked_outer_product(Wv, Wv)
    Wv_W2v = get_symmetric(stacked_outer_product(Wv, W2v))
    return concatenate_tensors(v_Wv, Wv_Wv, Wv_W2v, ndim=W.ndim + 1)


@register_invariant(
    3, TD(1, None), TD(1, None), num_invariants=1, output_def=TD(2, "s")
)
def O3_2s_vu(v, u):
    return stacked_outer_product(v, u)


@register_invariant(
    3, TD(2, "s"), TD(1, None), TD(1, None), num_invariants=1, output_def=TD(2, "s")
)
def O3_2s_Avu(A, v, u):
    v_ua = get_antisymmetric(stacked_outer_product(v, u))
    return get_symmetric(A @ v_ua)


@register_invariant(
    3, TD(2, "a"), TD(1, None), TD(1, None), num_invariants=1, output_def=TD(2, "s")
)
def O3_2s_Wvu(W, v, u):
    v_ua = get_antisymmetric(stacked_outer_product(v, u))
    return get_symmetric(W @ v_ua)


register_invariant(
    3,
    TD(2, "s"),
    TD(2, "s"),
    TD(2, "s"),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "s"),
    TD(2, "s"),
    TD(2, "a"),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "s"),
    TD(2, "a"),
    TD(2, "a"),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "a"),
    TD(2, "a"),
    TD(2, "a"),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "s"),
    TD(2, "s"),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "s"),
    TD(2, "a"),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "a"),
    TD(2, "a"),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "s"),
    TD(2, "s"),
    TD(1, None),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "s"),
    TD(2, "a"),
    TD(1, None),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(2, "a"),
    TD(2, "a"),
    TD(1, None),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)
register_invariant(
    3,
    TD(1, None),
    TD(1, None),
    TD(1, None),
    num_invariants=0,
    output_def=TD(2, "s"),
    invariant_point_map=False,
)

#### End Table 2 ####
#####################


@register_invariant(2, TD(1, None), num_invariants=1)
def SO2_v(v):
    return stacked_dot(v, v)


@register_invariant(2, TD(2, "s"), num_invariants=2)
def SO2_A(A):
    A2 = A @ A
    a = np.trace(A, axis1=1, axis2=2)
    b = np.trace(A2, axis1=1, axis2=2)
    return np.column_stack((a, b))


# TODO: this is for a transversely isotropic thing.
@register_invariant(2, TD(2, "s"), TD(1, None), num_invariants=2)
def SO2_Av(A, v):
    Av = stacked_matvec(A, v)
    A2v = stacked_matvec(A, Av)

    a = stacked_dot(v, Av)
    b = stacked_dot(Av, Av)
    # c = scalar_triple_product(v, Av, A2v)
    return np.column_stack((a, b))


@register_invariant(2, num_invariants=1, output_def=TD(2, "s"))
def SO2_2s_():
    return coo(np.eye(2)[None, ...])


@register_invariant(2, TD(2, "s"), num_invariants=2, output_def=TD(2, "s"))
def SO2_2s_A(A):
    A2 = A @ A
    return concatenate_tensors(A, A2, ndim=A.ndim + 1)


@register_invariant(2, TD(1, None), num_invariants=1, output_def=TD(2, "s"))
def SO2_2s_v(v):
    return stacked_outer_product(v, v)


@register_invariant(2, TD(2, "s"), TD(1, None), num_invariants=2, output_def=TD(2, "s"))
def SO2_2s_Av(A, v):
    Av = stacked_matvec(A, v)
    A2v = stacked_matvec(A, Av)
    v_Av = get_symmetric(stacked_outer_product(v, Av))
    v_A2v = get_symmetric(stacked_outer_product(v, A2v))
    return concatenate_tensors(v_Av, v_A2v, ndim=A.ndim + 1)
